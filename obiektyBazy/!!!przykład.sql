SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[_imag_twit_zakladanie_artykulu]
@IdObiektu numeric, @Wynik1 varchar(255) OUTPUT
as 
  begin
    set xact_abort on 
    set transaction isolation level REPEATABLE READ 
    begin transaction  
	declare @id_artykulu numeric 
	set @id_artykulu = @IdObiektu
	declare @id_magazynu2 numeric
	declare @id_firmy2 numeric
	declare @id_artykulu2 numeric
	declare @id_kategorii2 numeric
	declare @id_jednostki2 numeric
	declare @id_kategorii_tree2 numeric
	declare @guid_kategoria UNIQUEIDENTIFIER
	declare @nazwa_kategorii varchar(50)
	declare @rodzaj varchar(10)
	declare @rodzaj2 varchar(10)
	declare @stan_magazynu2 varchar(20)
	declare @id_mag numeric
	declare @id_firmy numeric
	declare @id_jednostki numeric
	declare @id_kategorii numeric
	declare @guid_artykul UNIQUEIDENTIFIER
	declare @indeks_kat varchar(30)
	declare @wspolne_ceny tinyint
	declare @wyroznik_synchro varchar(20)
	declare @nazwa_mag varchar(50)
	declare @stan tinyint
	declare @zamowienia tinyint
	declare @errmsg varchar(255)
	declare @kod_kreskowy varchar(20)


--   if 'Wsp�lne dane i ceny podstawowe'=(select top 1 wartosc from INTERFIRMAPOZYCJA IFP,POZYCJAKONFIG PK,artykul a,magazyn m
--                  where IFP.id_firmy=m.id_firmy and m.id_magazynu=a.id_magazynu and a.id_artykulu=@id_artykulu
--                   and IFP.id_pozycji=PK.id_pozycji and PK.kod='SynchroArt')  
	select @wspolne_ceny = 1
--   else
--      select @wspolne_ceny = 0

	select top 1
		@id_kategorii  = a.id_kategorii,
        @id_mag        = a.id_magazynu,
        @id_jednostki  = a.id_jednostki,
        @id_artykulu   = a.id_artykulu,
        @id_firmy      = m.id_firmy,
        @wyroznik_synchro = m.wyroznik_synchronizacji,
        @guid_artykul  = a.guid_artykul,
        @indeks_kat    = a.indeks_katalogowy,
        @rodzaj        = a.rodzaj,
		@kod_kreskowy  = a.KOD_KRESKOWY
	from artykul a inner join magazyn m on (m.id_magazynu=a.id_magazynu)
	where  a.id_artykulu=@id_artykulu

	if @id_mag not in (1, 2) begin goto Error end

	select top 1 @guid_kategoria = guid_kategoria, @nazwa_kategorii = nazwa
	from KATEGORIA_ARTYKULU
	where ID_KATEGORII = @id_kategorii

  
	select @nazwa_mag = ''
       
	declare magazyny_cursor cursor local fast_forward for
	select m.id_magazynu, m.nazwa, m.STAN_MAGAZYNU, m.ID_FIRMY
	from magazyn m
	where m.id_magazynu = case when @id_mag = 1 then 2 else 1 end--<>@id_mag
	/*and m.id_firmy=@id_firmy and m.wyroznik_synchronizacji=@wyroznik_synchro*/
	order by m.id_magazynu
   
	open magazyny_cursor

	While 1=1
	begin
		fetch next from magazyny_cursor into @id_magazynu2, @nazwa_mag, @stan_magazynu2, @id_firmy2

		if @@FETCH_STATUS <> 0 break

		select @id_kategorii2 = null, @id_artykulu2 = null
--print 'check 1 @id_artykulu = ' + cast(@id_artykulu as varchar(20)) + ' ; @id_magazyny2 = ' + cast(@id_magazynu2 as varchar(20))
		select top 1 @id_kategorii_tree2 = id_kategorii_tree from KATEGORIA_ARTYKULU_TREE with (nolock) where ID_MAGAZYNU = @id_magazynu2 and POZIOM = 0
--print 'check 2'
		-- sprawd� czy istnieje w tym magazynie kategoria 
		select top 1 @id_kategorii2 = ID_KATEGORII
		from KATEGORIA_ARTYKULU
		where ID_MAGAZYNU = @id_magazynu2 and guid_kategoria = @guid_kategoria
--print 'check 3'
		if isnull(@id_kategorii2,0) = 0
			select top 1 @id_kategorii2 = ID_KATEGORII
			from KATEGORIA_ARTYKULU
			where ID_MAGAZYNU = @id_magazynu2 
			and nazwa = @nazwa_kategorii
--print 'check 4'

		if isnull(@id_kategorii2,0) = 0
		begin
	        -- dodaj kategori� je�li brak
			insert into KATEGORIA_ARTYKULU
				(ID_KATEGORII, ID_MAGAZYNU, KOD_VAT, NAZWA, NUMER_DZIALU, MASKA_I_KATALOG, MASKA_I_HANDL,KONTO_FK_KATEGORII,GUID_KATEGORIA,KOD_VAT_ZAK)
			select (select max(ID_KATEGORII) from KATEGORIA_ARTYKULU)+1,
				@id_magazynu2, KOD_VAT, NAZWA, NUMER_DZIALU, MASKA_I_KATALOG, MASKA_I_HANDL,KONTO_FK_KATEGORII,GUID_KATEGORIA,KOD_VAT_ZAK
			from KATEGORIA_ARTYKULU
			where ID_KATEGORII = @id_kategorii
    
			select top 1 @id_kategorii2 = ID_KATEGORII
			from KATEGORIA_ARTYKULU
			where ID_MAGAZYNU = @id_magazynu2
			and guid_kategoria = @guid_kategoria

			if not exists (select 1 from NUMERACJA where 
                typ_dokumentu = @id_kategorii2 and id_magazynu = @id_magazynu2 and (dokument = 5 or dokument = 6))       
			INSERT INTO NUMERACJA (DOKUMENT,ID_FIRMY,ID_MAGAZYNU,LAST_ID,OKRES,TYP_DOKUMENTU) 
			select DOKUMENT,@id_firmy2/*ID_FIRMY*/,@id_magazynu2,LAST_ID,OKRES,@id_kategorii2
			from numeracja where typ_dokumentu = @id_kategorii  and id_magazynu = @id_mag and (dokument = 5 or dokument = 6)           

			insert into CENA_KATEGORII(ID_KATEGORII, ID_CENY, DOMYSLNY_NARZUT)
			select @id_kategorii2, C.ID_CENY, 0
			from CENA C
			where ID_FIRMY = @id_firmy2
			and not exists (select 1 from CENA_KATEGORII CK where C.ID_CENY = C.ID_CENY)

		end
		
		set @id_jednostki2 = null
		set @id_artykulu2 = null
		set @indeks_kat = null
		set @rodzaj2 = null  
		
		select top 1
			@id_jednostki2 = id_jednostki,
			@id_artykulu2 = id_artykulu,
			@indeks_kat = indeks_katalogowy,
			@rodzaj2 = Rodzaj   
		from artykul where id_magazynu = @id_magazynu2 and guid_artykul = @guid_artykul

print 'check 5'   
		if isnull(@id_artykulu2,0) = 0
		begin
			update artykul set
				guid_artykul = @guid_artykul
			where  id_magazynu = @id_magazynu2 and indeks_katalogowy = @indeks_kat

			if @@rowcount>0
				select top 1
					@id_jednostki2 = id_jednostki,
					@id_artykulu2 = id_artykulu,
					@indeks_kat = indeks_katalogowy,
					@rodzaj2 = Rodzaj   
				from artykul where id_magazynu = @id_magazynu2 and guid_artykul = @guid_artykul
		end
print 'check 6'               
		if isnull(@id_artykulu2,0)>0
		begin
			select @stan=0,@zamowienia=0
			select top 1 @stan=1 from pozycja_dokumentu_magazynowego where id_artykulu=@id_artykulu2
			select top 1 @zamowienia=1 from pozycja_zamowienia where id_artykulu=@id_artykulu2
		end
print 'check 7'
		if isnull(@id_artykulu2,0)=0
		begin

			-- dodanie artyku�u
			if exists (select 1 from artykul where id_magazynu=@id_magazynu2 and guid_artykul<>@guid_artykul and indeks_katalogowy=@indeks_kat)
			begin
				select @errmsg = 'W magazynie ' +rtrim(@nazwa_mag) + ' istnieje inny artyku� o takim samym indeksie katalogowym!. Synchronizacja nie mo�e zosta� przeprowadzona dla artyku�u o indeksie '+rtrim(@indeks_kat)
				goto error
			end

			insert into ARTYKUL (
				ID_ARTYKULU,
				ID_KATEGORII,
				ID_KATEGORII_TREE,
				ID_MAGAZYNU,
				ID_JEDNOSTKI,
				ID_JEDNOSTKI_ZAK,
				ID_JEDNOSTKI_SPRZ,
				VAT_ZAKUPU,
				VAT_SPRZEDAZY,
				NAZWA,
				NAZWA2,
				STAN,
				ILOSC_EDYTOWANA,
				ZAREZERWOWANO,
				OD_DOSTAWCOW,
				DO_ODBIORCOW,
				INDEKS_KATALOGOWY,
				INDEKS_HANDLOWY,
				RODZAJ,
				PLU,
				SWWKU,
				PKWiU,
				STAN_MINIMALNY,
				STAN_MAKSYMALNY,
				CENA_ZAKUPU_NETTO,
				CENA_ZAKUPU_BRUTTO,
				ID_CENY_DOM,
				CERTYFIKAT,
				DATA_CERTYFIKATU,
				NAZWA_CERTYFIKATU,
				OPIS,
				UWAGI,
				SEMAFOR,
				FLAGA_STANU,
				ID_ARTYKULU_PROD,
				JEDNOSTKA_PROD,
				PRZELICZNIK_PROD,
				ILOSC_PROD,
				NAZWA_ORYG,
				WAGA,
				WYMIAR_W,
				WYMIAR_S,
				WYMIAR_G,
				KOD_KRESKOWY,	
				WYROZNIK,
				POLE1,
				POLE2,
				POLE3,
				POLE4,
				POLE5,
				POLE6,
				POLE7,
				POLE8,
				POLE9,
				POLE10,
				JED_WAGI,
				JED_WYMIARU,
				KRAJ_POCHODZENIA,
				OSTRZEZENIE,        
				POKAZUJ_OSTRZEZENIE,
				guid_artykul,
				PODLEGA_RABATOWANIU,
				KOD_CN,
				NAZWA_INTRASTAT,
				ZABLOKOWANY,
				AKT_CEN_PRZY_DOSTAWIE,
				ID_JEDNOSTKI_REF,
				ID_OPAKOWANIA_REF,
				PROMOCJA_OD,
				PROMOCJA_DO,
				CENA_PROMOCJI_N,
				CENA_PROMOCJI_B,
				WYLACZ_CENY_IND,
				CENA_N_KGO,
				CENA_B_KGO,
				PROMOCJA_RABAT,
				PRODUCENT,
				ID_FPROM,
				LOKALIZACJA,
				PRG_LOJAL,
				DATA_ZM_CZ,
				IND_KAT_TOWAR_ZAST,
				C_ZAKUPU_NETTO_WAL,
				C_ZAKUPU_BRUTTO_WAL,
				SYM_WAL,
				MARZOWY,
				AKCYZA,
				AKCYZA_JM,
				AKCYZA_PRZELICZNIK,
				AKCYZA_STAWKA_ZA_JM,
				AKCYZA_PRZELICZNIK_JM_JA,
				ID_DOSTAWCY_PREFEROWANEGO,
				ID_PRODUCENTA,
				DOSTEPNY_W_SKLEPIE_INTER,
				JEST_ZDJECIE
			)
			select
				(select max(ID_ARTYKULU)+1 from ARTYKUL),
				@id_kategorii2,
				@id_kategorii_tree2,
				@id_magazynu2,
				ID_JEDNOSTKI,
				0,
				0,
				VAT_ZAKUPU,
				VAT_SPRZEDAZY,
				NAZWA,
				NAZWA2,
				0,
				0,
				0,
				0,
				0,
				INDEKS_KATALOGOWY,
				INDEKS_HANDLOWY,
				RODZAJ,
				PLU,
				SWWKU,
				PKWiU,
				0,--STAN_MINIMALNY,
				0,--STAN_MAKSYMALNY,
				cena_zakupu_netto,
				cena_zakupu_brutto,
				ID_CENY_DOM,
				CERTYFIKAT,
				DATA_CERTYFIKATU,
				NAZWA_CERTYFIKATU,
				OPIS,
				UWAGI,
				SEMAFOR,
				201,
				ID_ARTYKULU_PROD,
				JEDNOSTKA_PROD,
				PRZELICZNIK_PROD,
				ILOSC_PROD,
				NAZWA_ORYG,
				WAGA,
				WYMIAR_W,
				WYMIAR_S,
				WYMIAR_G,
				KOD_KRESKOWY,
				WYROZNIK,
				POLE1,
				POLE2,
				POLE3,
				POLE4,
				POLE5,
				POLE6,
				POLE7,
				POLE8,
				POLE9,
				POLE10,
				JED_WAGI,
				JED_WYMIARU,
				KRAJ_POCHODZENIA,
				OSTRZEZENIE,        
				POKAZUJ_OSTRZEZENIE, 
				guid_artykul,
				PODLEGA_RABATOWANIU,
				KOD_CN,
				NAZWA_INTRASTAT,
				ZABLOKOWANY,
				AKT_CEN_PRZY_DOSTAWIE,
				0, -- ID_JEDNOSTKI_REF
				0, -- ID_OPAKOWANIA_REF
				PROMOCJA_OD,
				PROMOCJA_DO,
				CENA_PROMOCJI_N,
				CENA_PROMOCJI_B,
				WYLACZ_CENY_IND,
				CENA_N_KGO,
				CENA_B_KGO,
				PROMOCJA_RABAT,
				PRODUCENT,
				ID_FPROM,
				LOKALIZACJA,
				PRG_LOJAL,
				0,
				IND_KAT_TOWAR_ZAST,
				C_ZAKUPU_NETTO_WAL,
				C_ZAKUPU_BRUTTO_WAL,
				SYM_WAL,
				MARZOWY,
				AKCYZA,
				AKCYZA_JM,
				AKCYZA_PRZELICZNIK,
				AKCYZA_STAWKA_ZA_JM,
				AKCYZA_PRZELICZNIK_JM_JA,
				ID_DOSTAWCY_PREFEROWANEGO,
				ID_PRODUCENTA,
				DOSTEPNY_W_SKLEPIE_INTER,
				JEST_ZDJECIE
			from ARTYKUL
			where ID_ARTYKULU = @id_artykulu

			select top 1 @id_artykulu2 = ID_ARTYKULU
			from ARTYKUL
			where ID_MAGAZYNU = @id_magazynu2 and guid_artykul = @guid_artykul

			-- Skopiowanie innych jednostek
			insert into JEDNOSTKA(ID_ARTYKULU, ID_FIRMY, NAZWA, SKROT, PRZELICZNIK, PODZIELNA)
			select @id_artykulu2, @id_firmy2, j.NAZWA, j.SKROT, j.PRZELICZNIK, j.PODZIELNA
			from JEDNOSTKA j
			where j.ID_ARTYKULU = @id_artykulu

print 'check 8 @id_magazynu2 = ' + cast(@id_magazynu2 as varchar(20)) + ' @id_firmy2 = ' + cast(@id_firmy2 as varchar(20))

			-- Skopiowanie kod�w kreskowych
			insert into KOD_KRESKOWY(ID_MAGAZYNU,KOD_KRESKOWY,ID_ARTYKULU,ID_JEDNOSTKI,OPIS,PRZENOS_OPIS,DOM_ILOSC)
			select @id_magazynu2, KOD_KRESKOWY, @id_artykulu2, 
			(select top 1 id_jednostki from jednostka where skrot = (select top 1 SKROT from jednostka where ID_JEDNOSTKI = k.ID_JEDNOSTKI) and ID_FIRMY = @id_firmy2), 
			OPIS, PRZENOS_OPIS, DOM_ILOSC
			from KOD_KRESKOWY k
			where k.ID_ARTYKULU = @id_artykulu 
			and KOD_KRESKOWY not in (select kod_kreskowy from kod_kreskowy where id_magazynu=@id_magazynu2)

			set @id_jednostki2 = null
			select top 1 
				@id_jednostki2 = id_jednostki 
			from JEDNOSTKA J2
			where ID_ARTYKULU is null
			and ID_FIRMY = @id_firmy2 
			and SKROT in (select SKROT from jednostka j where j.ID_JEDNOSTKI = @id_jednostki)

			if isNull(@id_jednostki2, 0) = 0
			begin
				insert into JEDNOSTKA(ID_ARTYKULU, ID_FIRMY, NAZWA, SKROT, PRZELICZNIK, PODZIELNA)
				select null, @id_firmy2, j.NAZWA, j.SKROT, j.PRZELICZNIK, j.PODZIELNA
				from JEDNOSTKA j
				where j.ID_JEDNOSTKI = @id_jednostki

				select top 1 
					@id_jednostki2 = id_jednostki 
				from JEDNOSTKA J2
				where ID_ARTYKULU is null
				and ID_FIRMY = @id_firmy2 
				and exists (select 1 from jednostka j where j.ID_FIRMY=@id_firmy and j.ID_JEDNOSTKI = @id_jednostki and j.NAZWA =J2.NAZWA
							and j.SKROT = J2.SKROT and j.PRZELICZNIK = J2.PRZELICZNIK and j.PODZIELNA = J2.PODZIELNA)

			end
			else
			begin
				update j2 set
					nazwa = j.NAZWA,
					przelicznik = j.PRZELICZNIK, 
					podzielna = j.PODZIELNA
				from JEDNOSTKA j, JEDNOSTKA j2
				where j.ID_JEDNOSTKI = @id_jednostki
				and j2.ID_JEDNOSTKI = @id_jednostki2
			end

 
		    -- Uzupe�nienie danych w artykule o jednostkach domyslnych
			Update ARTYKUL set 
				ID_JEDNOSTKI = @id_jednostki2,
				ID_JEDNOSTKI_ZAK = isnull((select j.ID_JEDNOSTKI 
										from JEDNOSTKA j 
										where j.ID_FIRMY  = @id_firmy2
										and j.ID_ARTYKULU = @id_artykulu2
										and j.SKROT = (select jj.SKROT
													   from JEDNOSTKA jj, ARTYKUL aa
													   where jj.ID_JEDNOSTKI = aa.ID_JEDNOSTKI_ZAK
													   and aa.ID_ARTYKULU = @id_artykulu)),0),
				ID_JEDNOSTKI_SPRZ = isnull((select j.ID_JEDNOSTKI 
										from JEDNOSTKA j 
										where j.ID_FIRMY  = @id_firmy2
										and j.ID_ARTYKULU = @id_artykulu2
										and j.SKROT = (select jj.SKROT
													   from JEDNOSTKA jj, ARTYKUL aa
													   where jj.ID_JEDNOSTKI = aa.ID_JEDNOSTKI_SPRZ
													   and aa.ID_ARTYKULU = @id_artykulu)),0),
				ID_JEDNOSTKI_REF = isnull((select j.ID_JEDNOSTKI 
										from JEDNOSTKA j 
										where j.ID_FIRMY  = @id_firmy2
										and j.ID_ARTYKULU = @id_artykulu2
										and j.SKROT = (select jj.SKROT
													   from JEDNOSTKA jj, ARTYKUL aa
													   where jj.ID_JEDNOSTKI = aa.ID_JEDNOSTKI_REF
													   and aa.ID_ARTYKULU = @id_artykulu)),0)
			where ID_ARTYKULU = @id_artykulu2

			insert CENA_ARTYKULU(ID_ARTYKULU, ID_CENY, CENA_NETTO, CENA_BRUTTO, UWAGI, SYM_WAL,OBL_WG_CEN_ZAK,PRZELICZNIK_WAL)
			select @id_artykulu2, c.ID_CENY, 0.0000, 0.0000, '', '', 0, 0.0000
			from CENA C
			where C.ID_FIRMY = @id_firmy2

		end
		else
		begin  
			if @stan_magazynu2<>'Praca'
			begin
				select @errmsg = 'Magazyn ' +rtrim(@nazwa_mag) + ' jest w stanie remanentu/bilansu. Synchronizacja artyku�u o indeksie katalogowym ' + rtrim(@indeks_kat)+' nie mo�e zosta� wykonana.'
				goto error
			end
	   
			if @rodzaj<>@rodzaj2 and isnull(@rodzaj,'')<>'' and (@stan=1 or @zamowienia=1) 
			begin
				select @errmsg = 'Zmiana rodzaju artyku�u w magazynie ' +rtrim(@nazwa_mag) + ' o indeksie katalogowym ' + rtrim(@indeks_kat) + ' i jego synchronizacja nie mo�e by� przeprowadzona poniewa� istniej� dokumenty, na kt�rych u�yty by� ten artyku�.'
				goto error
			end

			-- Skopiowanie kod�w kreskowych
			insert into KOD_KRESKOWY(ID_MAGAZYNU,KOD_KRESKOWY,ID_ARTYKULU,ID_JEDNOSTKI,OPIS,PRZENOS_OPIS,DOM_ILOSC)
			select @id_magazynu2, KOD_KRESKOWY, @id_artykulu2, 
			(select top 1 id_jednostki from jednostka where skrot = (select top 1 SKROT from jednostka where ID_JEDNOSTKI = k.ID_JEDNOSTKI) and ID_FIRMY = @id_firmy2), 
			OPIS, PRZENOS_OPIS, DOM_ILOSC
			from KOD_KRESKOWY k
			where k.ID_ARTYKULU = @id_artykulu 
			and KOD_KRESKOWY not in (select kod_kreskowy from kod_kreskowy where id_magazynu=@id_magazynu2)

			delete from KOD_KRESKOWY 
			where id_artykulu = @id_artykulu2 
			and kod_kreskowy not in (select kod_kreskowy from kod_kreskowy where id_artykulu=@id_artykulu)

print 'check 7.1'

			update kod_kreskowy set 
				dom_ilosc    = k.dom_ilosc,
				opis         = k.opis,
				przenos_opis = k.przenos_opis,
				ID_JEDNOSTKI = (select top 1 id_jednostki from jednostka where skrot = (select top 1 SKROT from jednostka where ID_JEDNOSTKI = k.ID_JEDNOSTKI) and ID_FIRMY = @id_firmy2)
			from kod_kreskowy kod, (select * from kod_kreskowy where id_magazynu=@id_mag and id_artykulu=@id_artykulu) k
			where kod.id_magazynu=@id_magazynu2 and kod.id_artykulu=@id_artykulu2 and kod.kod_kreskowy=k.kod_kreskowy


			update ARTYKUL set
				ID_KATEGORII 		= @id_kategorii2,
				ID_MAGAZYNU  		= @id_magazynu2
			where ID_ARTYKULU = @id_artykulu2

			-- Skopiowanie innych jednostek
			insert into JEDNOSTKA(ID_ARTYKULU, ID_FIRMY, NAZWA, SKROT, PRZELICZNIK, PODZIELNA)
			select @id_artykulu2, @id_firmy2, j.NAZWA, j.SKROT, j.PRZELICZNIK, j.PODZIELNA
			from JEDNOSTKA j
			where j.ID_ARTYKULU = @id_artykulu and 
			j.SKROT not in (select SKROT from jednostka jj
						where jj.ID_FIRMY=@id_firmy2 and jj.ID_ARTYKULU=@id_artykulu2)


			delete from jednostka where id_firmy =@id_firmy2 and id_artykulu=@id_artykulu2 
			and skrot not in (select skrot from jednostka where id_firmy=@id_firmy and id_artykulu=@id_artykulu)

			update jednostka set  
				przelicznik = jj.przelicznik,
				podzielna = jj.podzielna,
				nazwa = jj.nazwa
			from jednostka j,(select * from jednostka where id_firmy=@id_firmy and id_artykulu =@id_artykulu) jj
			where j.id_firmy=@id_firmy2 and j.id_artykulu=@id_artykulu2 and j.skrot = jj.skrot


			set @id_jednostki2 = null
			select top 1 
				@id_jednostki2 = id_jednostki 
			from JEDNOSTKA J2
			where ID_ARTYKULU is null
			and ID_FIRMY = @id_firmy2 
			and SKROT in (select SKROT from jednostka j where j.ID_JEDNOSTKI = @id_jednostki)

			if isNull(@id_jednostki2, 0) = 0
			begin
				insert into JEDNOSTKA(ID_ARTYKULU, ID_FIRMY, NAZWA, SKROT, PRZELICZNIK, PODZIELNA)
				select null, @id_firmy2, j.NAZWA, j.SKROT, j.PRZELICZNIK, j.PODZIELNA
				from JEDNOSTKA j
				where j.ID_JEDNOSTKI = @id_jednostki

				select top 1 
					@id_jednostki2 = id_jednostki
				from JEDNOSTKA J2
				where ID_ARTYKULU is null
				and ID_FIRMY = @id_firmy2 
				and exists (select 1 from jednostka j where j.ID_FIRMY=@id_firmy and j.ID_JEDNOSTKI = @id_jednostki and j.NAZWA =J2.NAZWA
							and j.SKROT = J2.SKROT and j.PRZELICZNIK = J2.PRZELICZNIK and j.PODZIELNA = J2.PODZIELNA)

			end
			else
			begin
				update j2 set
					nazwa = j.NAZWA,
					przelicznik = j.PRZELICZNIK, 
					podzielna = j.PODZIELNA
				from JEDNOSTKA j, JEDNOSTKA j2
				where j.ID_JEDNOSTKI = @id_jednostki
				and j2.ID_JEDNOSTKI = @id_jednostki2
			end

			-- Uzupe�nienie danych w artykule o jednostkach domyslnych
			Update ARTYKUL set
				ID_JEDNOSTKI = @id_jednostki2,
				ID_JEDNOSTKI_ZAK = isnull((select j.ID_JEDNOSTKI 
                                from JEDNOSTKA j 
                                where j.ID_FIRMY  = @id_firmy2
                                and j.ID_ARTYKULU = @id_artykulu2
                                and j.SKROT = (select jj.SKROT
                                               from JEDNOSTKA jj, ARTYKUL aa
                                               where jj.ID_JEDNOSTKI = aa.ID_JEDNOSTKI_ZAK
                                               and aa.ID_ARTYKULU = @id_artykulu)),0),
				ID_JEDNOSTKI_SPRZ = isnull((select j.ID_JEDNOSTKI 
                                from JEDNOSTKA j 
                                where j.ID_FIRMY  = @id_firmy2
                                and j.ID_ARTYKULU = @id_artykulu2
                                and j.SKROT = (select jj.SKROT
                                               from JEDNOSTKA jj, ARTYKUL aa
                                               where jj.ID_JEDNOSTKI = aa.ID_JEDNOSTKI_SPRZ
                                               and aa.ID_ARTYKULU = @id_artykulu)),0),
				ID_JEDNOSTKI_REF = isnull((select j.ID_JEDNOSTKI 
                                from JEDNOSTKA j 
                                where j.ID_FIRMY  = @id_firmy2
                                and j.ID_ARTYKULU = @id_artykulu2
                                and j.SKROT = (select jj.SKROT
                                               from JEDNOSTKA jj, ARTYKUL aa
                                               where jj.ID_JEDNOSTKI = aa.ID_JEDNOSTKI_REF
                                               and aa.ID_ARTYKULU = @id_artykulu)),0),
				KOD_KRESKOWY = @kod_kreskowy
			where ID_ARTYKULU = @id_artykulu2

/*
			delete CENA_ARTYKULU
			from cena_artykulu ca
			where ca.id_artykulu=@id_artykulu2
			and ca.id_ceny not in (select id_ceny from cena_kategorii where id_kategorii=@id_kategorii2)   

			insert CENA_ARTYKULU(ID_ARTYKULU, ID_CENY, CENA_NETTO, CENA_BRUTTO, UWAGI, SYM_WAL,OBL_WG_CEN_ZAK,PRZELICZNIK_WAL)
			select @id_artykulu2, ca.ID_CENY, 0, 0, ca.UWAGI, ca.SYM_WAL, ca.OBL_WG_CEN_ZAK,ca.PRZELICZNIK_WAL
			from CENA_ARTYKULU ca,CENA_KATEGORII ck
			where ca.ID_ARTYKULU = @id_artykulu and ca.ID_CENY = ck.ID_CENY and ck.ID_KATEGORII = @id_kategorii2
            and ck.id_ceny not in (select id_ceny from cena_artykulu where id_artykulu=@id_artykulu2)
*/
      
		end   

		insert into CENA_KATEGORII (ID_KATEGORII, ID_CENY)
		select @id_kategorii2, ID_CENY from CENA_ARTYKULU where id_artykulu = @id_artykulu2
		and not exists(select 1 from CENA_KATEGORII where id_ceny in (select ID_CENY from CENA_ARTYKULU where id_artykulu = @id_artykulu2) and ID_KATEGORII = @id_kategorii2)

		if @wspolne_ceny=1
		begin
			update c1 set 
				sym_wal = c2.sym_wal,
                przelicznik_wal = c2.przelicznik_wal,
                obl_wg_cen_zak = c2.obl_wg_cen_zak,
                cena_netto = c2.cena_netto,
                cena_brutto = c2.cena_brutto,
                uwagi = c2.uwagi
			from cena_artykulu c1 inner join ARTYKUL_CENA_IE i on (c1.id_ceny=i.ID_CENY and i.ID_FIRMY=@id_firmy2 and i.SYNCHRONIZACJA=1)
			inner join cena ic1 on c1.ID_CENY = ic1.ID_CENY
			inner join (select ca.sym_wal, ca.przelicznik_wal, ca.obl_wg_cen_zak, ca.cena_netto, ca.cena_brutto, ca.uwagi, c.OPIS, ca.ID_ARTYKULU
					from cena_artykulu ca
					inner join cena c on ca.id_ceny = c.ID_CENY
					where id_artykulu = @id_artykulu) c2 on (ic1.OPIS = c2.OPIS)
			where c1.id_artykulu=@id_artykulu2 and c2.id_artykulu=@id_artykulu 
            and (c1.cena_netto<>c2.cena_netto or c1.cena_brutto<>c2.cena_brutto or
                c1.sym_wal<>c2.sym_wal or c1.przelicznik_wal<>c2.przelicznik_wal or 
				c1.uwagi<>c2.uwagi)
                
			update z1 set  
				CENA_NETTO=z2.CENA_NETTO,
				CENA_BRUTTO=z2.Cena_brutto,
				sym_wal = z2.sym_wal,
                przelicznik_wal = z2.przelicznik_wal,
				StatusObiektu = 2	            
			from cena_artykulu_zmiana z1,
				cena_artykulu_zmiana z2,
				cena_artykulu c1 inner join ARTYKUL_CENA_IE i on (c1.id_ceny=i.ID_CENY and i.ID_FIRMY=@id_firmy2 and i.SYNCHRONIZACJA=1)
				inner join cena ic1 on c1.ID_CENY = ic1.ID_CENY
				inner join (select ca.sym_wal, ca.przelicznik_wal, ca.obl_wg_cen_zak, ca.cena_netto, ca.cena_brutto, ca.uwagi, c.OPIS, ca.ID_ARTYKULU, ca.ID_CENA_ARTYKULU
						from cena_artykulu ca
						inner join cena c on ca.id_ceny = c.ID_CENY
						where id_artykulu = @id_artykulu) c2 on (ic1.OPIS = c2.OPIS)
			where c1.id_artykulu=@id_artykulu2 and c2.id_artykulu=@id_artykulu 
			and z1.id_cena_artykulu = c1.id_cena_artykulu      
			and z2.id_cena_artykulu = c2.id_cena_artykulu
				
		end

	end
  
	close magazyny_cursor
	deallocate magazyny_cursor   

	update artykul set
		FLAGA_STANU = 0,
	    SEMAFOR = null
	where GUID_ARTYKUL = @guid_artykul and FLAGA_STANU > 200
   
koniec:
  if @@trancount>0  
    commit transaction

  set transaction isolation level READ COMMITTED
  return 0


error:
  close magazyny_cursor
  deallocate magazyny_cursor
  
error1:  
  if @@trancount>0
    rollback tran

  set transaction isolation level READ COMMITTED
  set @Wynik1 = @errmsg
  raiserror (@errmsg,16,1)
  Return 1	

end
