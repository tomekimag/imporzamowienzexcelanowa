if exists (select 1 from sysobjects where name = 'IMAG_twit_dodaj_zamowienia_do_dostawcow' and type = 'P')
   drop procedure dbo.IMAG_twit_dodaj_zamowienia_do_dostawcow
go
create procedure dbo.IMAG_twit_dodaj_zamowienia_do_dostawcow
@IdFirmy numeric, @IdMagazynu numeric, @IdUzytkownika numeric, @IdKontrahenta numeric, @dataDokumentu datetime, @numerZamowienia varchar(30) output
as
begin

	set xact_abort on
	set transaction isolation level REPEATABLE READ
	begin transaction

	set @numerZamowienia = ''

	declare @NUMER varchar(100)

	declare @id_pracownika numeric

	select top 1 @id_pracownika = ID_PRACOWNIKA from PRACOWNIK where ID_UZYTKOWNIKA = @IdUzytkownika
	set @id_pracownika = isNull(@id_pracownika, 0)


	declare @Data Int
	declare @data_bez_czasu date
	set @data_bez_czasu = @dataDokumentu
	set @Data = cast(cast(@data_bez_czasu as datetime) as int) + 36163

	declare @ID_ARTYKULU numeric
	declare @KOD_VAT varchar(3)
	declare @ZAMOWIONO decimal(16, 6)
	declare @ZREALIZOWANO decimal(16, 6)
	declare @ZAREZERWOWANO decimal(16, 6)
	declare @DO_REZERWACJI decimal(16, 6)
	declare @CENA_NETTO decimal(14, 4)
	declare @CENA_BRUTTO decimal(14, 4)
	declare @CENA_NETTO_WAL decimal(14, 4)
	declare @CENA_BRUTTO_WAL decimal(14, 4)
	declare @PRZELICZNIK decimal(16, 6)
	declare @NARZUT decimal(8, 4)
	declare @OPIS varchar(500)
	declare @znak_narzutu tinyint
	declare @TRYB_REJESTRACJI tinyint
	declare @ID_DOSTAWY_REZ numeric
	declare @ID_WARIANTU_PRODUKTU numeric
	declare @ZNACZNIK_CENY char(1)

	declare @ID_JEDNOSTKI numeric

	declare @RetStat int

	declare @POLE1 varchar(100)
	declare @POLE1_pozycja varchar(100), @POLE8_pozycja varchar(100)

	declare @id_pozycji_zamowienia numeric, @id_zamowienia numeric

	declare @kod_katalogowy varchar(20), @kod_handlowy varchar(20), @nazwa varchar(40), @jednostka varchar(10), @PRODUCENT varchar(50), @KRAJ_POCHODZENIA varchar(3),
		@KATEGORIA varchar(50), @KATEGORIA_WIELOPOZIOMOWA varchar(50), @JEDNOSTKA_SKROT varchar(10), @VAT_ZAKUPU char(3), @VAT_SPRZEDAZY char(3), @STAN_MINIMALNY decimal(16, 6),
		@STAN_MAKSYMALNY decimal(16, 6), @JED_WAGI varchar(10), @WAGA decimal(20, 3), @JED_WYMIARU varchar(10), @WYMIAR_W decimal(20, 3), @WYMIAR_S decimal(20, 3), @WYMIAR_G decimal(20, 3),
		@ilosc_sztuk decimal(16, 6), @pole1_karton varchar(100), @PART varchar(100)
	
	exec RM_DodajZamowienie_Server @IdFirmy, @IdKontrahenta, @IdMagazynu, 2, @Data, @IdUzytkownika, 0, 'Netto', 1, @id_zamowienia output

	declare kursor_po_poz cursor local fast_forward for
	select diz.id_artykulu, diz.kod_katalogowy, diz.kod_handlowy, diz.nazwa, diz.jednostka, diz.PRODUCENT, diz.KRAJ_POCHODZENIA,
		diz.KATEGORIA, diz.KATEGORIA_WIELOPOZIOMOWA, diz.JEDNOSTKA_SKROT, diz.VAT_ZAKUPU, diz.VAT_SPRZEDAZY, diz.STAN_MINIMALNY,
		diz.STAN_MAKSYMALNY, diz.JED_WAGI, diz.WAGA, diz.JED_WYMIARU, diz.WYMIAR_W, diz.WYMIAR_S, diz.WYMIAR_G, 
		a.ID_JEDNOSTKI, a.VAT_ZAKUPU,
		diz.ilosc_sztuk, diz.pole1_karton, diz.PART
	from _imag_dane_importu_zam_od_odbiorcy diz 
	join ARTYKUL a on diz.id_artykulu = a.ID_ARTYKULU
	where diz.id_uzytkownika = @IdUzytkownika
		
	OPEN kursor_po_poz
	
	FETCH NEXT FROM kursor_po_poz into @id_artykulu, @kod_katalogowy, @kod_handlowy, @nazwa, @jednostka, @PRODUCENT, @KRAJ_POCHODZENIA,
		@KATEGORIA, @KATEGORIA_WIELOPOZIOMOWA, @JEDNOSTKA_SKROT, @VAT_ZAKUPU, @VAT_SPRZEDAZY, @STAN_MINIMALNY,
		@STAN_MAKSYMALNY, @JED_WAGI, @WAGA, @JED_WYMIARU, @WYMIAR_W, @WYMIAR_S, @WYMIAR_G, @ID_JEDNOSTKI, @KOD_VAT, @ilosc_sztuk,
		@pole1_karton, @PART 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN

		

		select top 1 @JEDNOSTKA = SKROT from JEDNOSTKA where ID_JEDNOSTKI = @ID_JEDNOSTKI

		set @ZAMOWIONO = @ilosc_sztuk
		set @ZREALIZOWANO = 0
		set @NARZUT = 0
		set @OPIS = ''
		set @znak_narzutu = 2
		set @TRYB_REJESTRACJI = 0
		set @ID_DOSTAWY_REZ = 0
		set @ID_WARIANTU_PRODUKTU = 0
		set @ZNACZNIK_CENY = 0
		set @DO_REZERWACJI = 0
		set @ZAREZERWOWANO = 0
		set @CENA_NETTO = 0
		set @CENA_BRUTTO = 0
		set @CENA_NETTO_WAL = 0
		set @CENA_BRUTTO_WAL = 0
		set @PRZELICZNIK = 1
		set @ID_WARIANTU_PRODUKTU = 0

		exec @RetStat = RM_DodajPozycjeZamowienia_Server @id_pozycji_zamowienia output, @id_zamowienia, @ID_ARTYKULU, @KOD_VAT, @ZAMOWIONO,
			@ZREALIZOWANO, @ZAREZERWOWANO, @DO_REZERWACJI, @CENA_NETTO, @CENA_BRUTTO, @CENA_NETTO_WAL,
			@CENA_BRUTTO_WAL, @PRZELICZNIK, @JEDNOSTKA_SKROT, @NARZUT, @OPIS, @znak_narzutu, @TRYB_REJESTRACJI, 
			@ID_DOSTAWY_REZ, @ID_WARIANTU_PRODUKTU, @ZNACZNIK_CENY

		if @RetStat = 0 goto Error

		update POZYCJA_ZAMOWIENIA set
			POLE5 = @PART--,
	--		POLE1 = @pole1_karton
		where ID_POZYCJI_ZAMOWIENIA = @id_pozycji_zamowienia
			


		FETCH NEXT FROM kursor_po_poz into @id_artykulu, @kod_katalogowy, @kod_handlowy, @nazwa, @jednostka, @PRODUCENT, @KRAJ_POCHODZENIA,
			@KATEGORIA, @KATEGORIA_WIELOPOZIOMOWA, @JEDNOSTKA_SKROT, @VAT_ZAKUPU, @VAT_SPRZEDAZY, @STAN_MINIMALNY,
			@STAN_MAKSYMALNY, @JED_WAGI, @WAGA, @JED_WYMIARU, @WYMIAR_W, @WYMIAR_S, @WYMIAR_G, @ID_JEDNOSTKI, @KOD_VAT, @ilosc_sztuk,
			@pole1_karton, @PART
	END

	CLOSE kursor_po_poz
	DEALLOCATE kursor_po_poz


	declare @suma_netto decimal(16, 4) 
	declare @suma_brutto decimal(16, 4)  
	declare @suma_netto_wal decimal(16, 4)  
	declare @suma_brutto_wal decimal(16, 4)  

	exec RM_SumujZamowienie_Server @id_zamowienia, 
		@suma_netto output, @suma_brutto output, @suma_netto_wal output, @suma_brutto_wal output

	update ZAMOWIENIE set
		WARTOSC_BRUTTO = @suma_brutto,
		WARTOSC_NETTO = @suma_netto,
		WARTOSC_BRUTTO_WAL = @suma_brutto_wal,
		WARTOSC_NETTO_WAL = @suma_netto_wal
	where ID_ZAMOWIENIA = @id_zamowienia


	declare @format_numeracji varchar(50)
	declare @okresnumeracji tinyint
	declare @parametr1 tinyint
	declare @parametr2 tinyint

	declare @id_typu int

	select top 1 @id_typu = id_typu from TYP_DOKUMENTU_MAGAZYNOWEGO where NAZWA = 'Zamówienie do dostawcy' and ID_FIRMY = @IdFirmy

	exec JL_PobierzFormatNumeracji_Server @IdFirmy, 2, @id_typu, @IdMagazynu, 
		@format_numeracji output, @okresnumeracji output, @parametr1 output, @parametr2 output

	declare @autonumer int

	exec RM_ZatwierdzZamowienie_Server @id_zamowienia, @IdKontrahenta, @id_typu, '<auto>', @format_numeracji,
			@okresnumeracji, @parametr1, @parametr2, @autonumer, @IdFirmy, @IdMagazynu, @Data, @Data, 0, 0, 2, 
			'', 2, null,
			1, 0, '', 0, 1, 0, 0.00, 2, '', '', '', 0, '', 0


	select TOP 1 @numerZamowienia = NUMER from ZAMOWIENIE where ID_ZAMOWIENIA = @id_zamowienia

	if @@trancount>0 commit transaction
	goto Koniec 
	Error: 
	IF (SELECT CURSOR_STATUS('global','kursor_po_poz')) >= -1
	BEGIN
		CLOSE kursor_po_poz
		DEALLOCATE kursor_po_poz
	END

	if @@trancount>0 rollback tran 
	goto Koniec 

	Koniec: 
		set transaction isolation level READ COMMITTED 
		return
	
end 
go
