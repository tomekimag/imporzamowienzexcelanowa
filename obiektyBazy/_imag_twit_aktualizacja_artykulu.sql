SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter procedure [dbo].[_imag_twit_aktualizacja_artykulu]
@id_uzytkownika numeric,
@wynik int OUTPUT
as 
  begin

	set @wynik = 1

    set xact_abort on 
    set transaction isolation level REPEATABLE READ 
    begin transaction  

	begin try

	declare @guid_kategoria UNIQUEIDENTIFIER
	declare @nazwa_kategorii varchar(50)
	declare @id_magazynu numeric
	declare @id_firmy numeric
	declare @id_jednostki numeric
	declare @id_kategorii numeric
	declare @guid_artykul UNIQUEIDENTIFIER
	declare @indeks_kat varchar(30)
	declare @wspolne_ceny tinyint
	declare @wyroznik_synchro varchar(20)
	declare @nazwa_mag varchar(50)
	declare @stan tinyint
	declare @zamowienia tinyint
	declare @errmsg varchar(255)
	declare @kod_kreskowy varchar(20)
	declare @id_kategorii_tree numeric

	declare @id_artykulu numeric, @kod_katalogowy varchar(20), @kod_handlowy varchar(20), @nazwa varchar(40), @jednostka varchar(10), @PRODUCENT varchar(50), @KRAJ_POCHODZENIA varchar(3),
		@KATEGORIA varchar(50), @KATEGORIA_WIELOPOZIOMOWA varchar(50), @JEDNOSTKA_SKROT varchar(10), @VAT_ZAKUPU char(3), @VAT_SPRZEDAZY char(3), @STAN_MINIMALNY decimal(16, 6),
		@STAN_MAKSYMALNY decimal(16, 6), @JED_WAGI varchar(10), @WAGA decimal(20, 3), @JED_WYMIARU varchar(10), @WYMIAR_W decimal(20, 3), @WYMIAR_S decimal(20, 3), @WYMIAR_G decimal(20, 3)

	declare kursor_po_poz cursor local fast_forward for
	select id_artykulu, kod_katalogowy, kod_handlowy, nazwa, jednostka, PRODUCENT, KRAJ_POCHODZENIA,
		KATEGORIA, KATEGORIA_WIELOPOZIOMOWA, JEDNOSTKA_SKROT, VAT_ZAKUPU, VAT_SPRZEDAZY, STAN_MINIMALNY,
		STAN_MAKSYMALNY, JED_WAGI, WAGA, JED_WYMIARU, WYMIAR_W, WYMIAR_S, WYMIAR_G
	from _imag_dane_importu_zam_od_odbiorcy
	where id_uzytkownika = @id_uzytkownika
		
	OPEN kursor_po_poz
	
	FETCH NEXT FROM kursor_po_poz into @id_artykulu, @kod_katalogowy, @kod_handlowy, @nazwa, @jednostka, @PRODUCENT, @KRAJ_POCHODZENIA,
		@KATEGORIA, @KATEGORIA_WIELOPOZIOMOWA, @JEDNOSTKA_SKROT, @VAT_ZAKUPU, @VAT_SPRZEDAZY, @STAN_MINIMALNY,
		@STAN_MAKSYMALNY, @JED_WAGI, @WAGA, @JED_WYMIARU, @WYMIAR_W, @WYMIAR_S, @WYMIAR_G
	
	WHILE @@FETCH_STATUS = 0
	BEGIN

		select top 1 @id_magazynu = ID_MAGAZYNU from ARTYKUL where ID_ARTYKULU = @id_artykulu

		select top 1 @id_firmy = ID_FIRMY from magazyn where id_magazynu = @id_magazynu

		select top 1 @id_kategorii = id_kategorii from KATEGORIA_ARTYKULU where ID_MAGAZYNU = @id_magazynu and nazwa = @KATEGORIA
	  
		select top 1 @id_kategorii_tree = id_kategorii_tree from KATEGORIA_ARTYKULU_TREE with (nolock) where ID_MAGAZYNU = @id_magazynu and NAZWA = @KATEGORIA_WIELOPOZIOMOWA

		select TOP 1 @id_jednostki = id_jednostki from jednostka with (nolock) where ID_FIRMY=1 and skrot = @JEDNOSTKA_SKROT


		update ARTYKUL set 
			INDEKS_HANDLOWY = @kod_handlowy,
			INDEKS_KATALOGOWY = @kod_katalogowy,
			NAZWA = @nazwa,
			VAT_ZAKUPU = @VAT_ZAKUPU,
			VAT_SPRZEDAZY=  @VAT_SPRZEDAZY,
			STAN_MINIMALNY = @STAN_MINIMALNY,
			STAN_MAKSYMALNY = @STAN_MAKSYMALNY,
			ID_JEDNOSTKI = @id_jednostki,
			ID_JEDNOSTKI_ZAK = @id_jednostki,
			ID_JEDNOSTKI_SPRZ = @id_jednostki,
			ID_JEDNOSTKI_REF = @id_jednostki
		where ID_ARTYKULU = @id_artykulu


		FETCH NEXT FROM kursor_po_poz into @id_artykulu, @kod_katalogowy, @kod_handlowy, @nazwa, @jednostka, @PRODUCENT, @KRAJ_POCHODZENIA,
			@KATEGORIA, @KATEGORIA_WIELOPOZIOMOWA, @JEDNOSTKA_SKROT, @VAT_ZAKUPU, @VAT_SPRZEDAZY, @STAN_MINIMALNY,
			@STAN_MAKSYMALNY, @JED_WAGI, @WAGA, @JED_WYMIARU, @WYMIAR_W, @WYMIAR_S, @WYMIAR_G
	END

	CLOSE kursor_po_poz
	DEALLOCATE kursor_po_poz

	end try
	begin catch
		set @errmsg = ERROR_MESSAGE()
		set @wynik = -1
		goto Error
	end catch
   
    if @@trancount>0 commit transaction 
    goto Koniec 
Error:
    raiserror (@errmsg,16,1) 
    if @@trancount>0 rollback tran 

	IF (SELECT CURSOR_STATUS('global','kursor_po_poz')) >= -1
	BEGIN
		CLOSE kursor_po_poz
		DEALLOCATE kursor_po_poz
	END

    goto Koniec 
Koniec: 
    set transaction isolation level READ COMMITTED 
    return  
end
