ALTER procedure [dbo].[_imag_twit_zwroc_artykul_gdy_istnieje]
@id_magazynu numeric, 
@kod_kreskowy varchar(20),
@kod_indywidualny varchar(20),
@kod_katalogowy varchar(20),
@part varchar(100),
@id_artykulu numeric OUTPUT,
@brak_kodu_EAN tinyint output,
@brak_kodu_indywidualnego tinyint output,
@brak_kodu_katalogowego tinyint output 
as 
begin

	set @id_artykulu = 0

	set @brak_kodu_EAN = 1
	set @brak_kodu_indywidualnego = 1
	set @brak_kodu_katalogowego = 1

	if (@kod_kreskowy != '')
		select TOP 1 @id_artykulu = ID_ARTYKULU from KOD_KRESKOWY where ID_MAGAZYNU = @id_magazynu and KOD_KRESKOWY = @kod_kreskowy

	--set @id_artykulu = ISNULL(@id_artykulu, 0)

	if isNull(@id_artykulu, 0) != 0
	begin
		set @brak_kodu_EAN = 0

		if isNull(@kod_indywidualny, '') != '' and exists(select 1
			from vLokalizacje L
			join KONTRAHENT K on L.idKontrahenta = K.ID_KONTRAHENTA
			join CENNIK_INDYWIDUALNY CI on K.ID_GRUPY = CI.ID_GRUPY 
			where L.lokalizacja = ltrim(rtrim(@part))
			and CI.ID_ARTYKULU = @id_artykulu
			and CI.INDEKS = @kod_indywidualny)
		begin
			set @brak_kodu_indywidualnego = 0
		end

		if isNull(@kod_katalogowy, '') != '' and exists (select 1 from ARTYKUL A where ID_ARTYKULU = @id_artykulu and INDEKS_KATALOGOWY = @kod_katalogowy)
		begin
			set @brak_kodu_katalogowego = 0
		end

	end
	else
	begin
		if isNull(@kod_indywidualny, '') != ''
			select TOP 1 @id_artykulu = CI.ID_ARTYKULU
			from vLokalizacje L
			join KONTRAHENT K on L.idKontrahenta = K.ID_KONTRAHENTA
			join CENNIK_INDYWIDUALNY CI on K.ID_GRUPY = CI.ID_GRUPY 
			where L.lokalizacja = ltrim(rtrim(@part))
			and rtrim(ltrim(CI.INDEKS)) = rtrim(ltrim(@kod_indywidualny))

		if isNull(@id_artykulu, 0) != 0
		begin
			set @brak_kodu_indywidualnego = 0

			if isNull(@kod_katalogowy, '') != '' and exists (select 1 from ARTYKUL A where ID_ARTYKULU = @id_artykulu and INDEKS_KATALOGOWY = @kod_katalogowy)
			begin
				set @brak_kodu_katalogowego = 0
			end			
		end
		else
		begin
			if isNull(@kod_katalogowy, '') != ''
				select TOP 1 @id_artykulu = ID_ARTYKULU from ARTYKUL A where A.ID_MAGAZYNU = @id_magazynu and INDEKS_KATALOGOWY = @kod_katalogowy

			if isNull(@id_artykulu, 0) != 0
			begin
				set @brak_kodu_katalogowego = 0
			end
		end

		set @id_artykulu = ISNULL(@id_artykulu, 0)

	end
end
