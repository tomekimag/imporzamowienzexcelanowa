drop table _imag_dane_importu_zam_od_odbiorcy
go
create table _imag_dane_importu_zam_od_odbiorcy
(
	id bigint not null identity(1, 1) primary key,
	id_uzytkownika numeric not null,
	id_artykulu numeric not null,
    kod_katalogowy varchar(20) null,
    kod_handlowy varchar(20) null,
	kod_EAN varchar(20) not null,
    nazwa varchar(40) null,
    jednostka varchar(10) null,
    pole1_karton varchar(100) null,
    PRODUCENT varchar(50) null,
	KRAJ_POCHODZENIA varchar(3) null,
    ilosc_sztuk decimal(16, 6) not null,
    cena_netto_domyslna_PLN decimal(14, 4) null,
    cena_USD decimal(14, 4) null,
    KATEGORIA varchar(50) null,
	KATEGORIA_WIELOPOZIOMOWA varchar(50) null,
	JEDNOSTKA_SKROT varchar(10) null,
	RODZAJ varchar(10) null,
	VAT_ZAKUPU char(3) null,
	VAT_SPRZEDAZY char(3) null,
    STAN_MINIMALNY decimal(16, 6) null,
    STAN_MAKSYMALNY decimal(16, 6) null,
	JED_WAGI varchar(10) null,
    WAGA decimal(20, 3) null,
    JED_WYMIARU varchar(10) null,
    WYMIAR_W decimal(20, 3) null,
	WYMIAR_S decimal(20, 3) null,
    WYMIAR_G decimal(20, 3) null,
    PART varchar(100) null
)
go
alter table _imag_dane_importu_zam_od_odbiorcy add idLokalizacji bigint null
go

--select * from _imag_dane_importu_zam_od_odbiorcy