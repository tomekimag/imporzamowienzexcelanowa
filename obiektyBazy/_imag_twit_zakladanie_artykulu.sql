USE [NOWASA2018]
GO
/****** Object:  StoredProcedure [dbo].[_imag_twit_zakladanie_artykulu]    Script Date: 30.08.2018 15:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[_imag_twit_zakladanie_artykulu]
@id_magazynu numeric, 
@KATEGORIA varchar(50),
@KATEGORIA_WIELOPOZIOMOWA varchar(50),
@JEDNOSTKA_SKROT varchar(10),
@VAT_ZAKUPU char(3),
@VAT_SPRZEDAZY char(3),
@nazwa varchar(40),
@kod_katalogowy varchar(20),
@kod_handlowy varchar(20),
@RODZAJ varchar(10),
@STAN_MINIMALNY decimal(16, 6),
@STAN_MAKSYMALNY decimal(16, 6),
@WAGA decimal(20, 3),
@JED_WAGI varchar(10),
@JED_WYMIARU varchar(10),
@WYMIAR_W decimal(20, 3),
@WYMIAR_S decimal(20, 3),
@WYMIAR_G decimal(20, 3),
@kod_EAN varchar(20),
@KRAJ_POCHODZENIA varchar(3),
@PRODUCENT varchar(50),

@kod_indywidualny varchar(20),
@part varchar(100),

@id_artykulu numeric OUTPUT,

@brak_kodu_EAN tinyint output,
@brak_kodu_indywidualnego tinyint output,
@brak_kodu_katalogowego tinyint output
as 
  begin
    set xact_abort on 
    set transaction isolation level REPEATABLE READ 
    begin transaction  

	declare @guid_kategoria UNIQUEIDENTIFIER
	declare @nazwa_kategorii varchar(50)
	declare @id_mag numeric
	declare @id_firmy numeric
	declare @id_jednostki numeric
	declare @id_kategorii numeric
	declare @guid_artykul UNIQUEIDENTIFIER
	declare @indeks_kat varchar(30)
	declare @wspolne_ceny tinyint
	declare @wyroznik_synchro varchar(20)
	declare @nazwa_mag varchar(50)
	declare @stan tinyint
	declare @zamowienia tinyint
	declare @errmsg varchar(255)
	declare @kod_kreskowy varchar(20)
	declare @id_kategorii_tree numeric

	select top 1 @id_firmy = ID_FIRMY from magazyn where id_magazynu = @id_magazynu

	select top 1 @id_kategorii = id_kategorii from KATEGORIA_ARTYKULU where ID_MAGAZYNU = @id_magazynu and nazwa = @KATEGORIA
	  
	select top 1 @id_kategorii_tree = id_kategorii_tree from KATEGORIA_ARTYKULU_TREE with (nolock) where ID_MAGAZYNU = @id_magazynu and NAZWA = @KATEGORIA_WIELOPOZIOMOWA

	select TOP 1 @id_jednostki = id_jednostki from jednostka with (nolock) where ID_FIRMY=1 and skrot = @JEDNOSTKA_SKROT
	
	set @guid_artykul = NEWID()
		
	insert into ARTYKUL (
				ID_ARTYKULU,
				ID_KATEGORII,
				ID_KATEGORII_TREE,
				ID_MAGAZYNU,
				ID_JEDNOSTKI,
				ID_JEDNOSTKI_ZAK,
				ID_JEDNOSTKI_SPRZ,
				VAT_ZAKUPU,
				VAT_SPRZEDAZY,
				NAZWA,
				NAZWA2,
				STAN,
				ILOSC_EDYTOWANA,
				ZAREZERWOWANO,
				OD_DOSTAWCOW,
				DO_ODBIORCOW,
				INDEKS_KATALOGOWY,
				INDEKS_HANDLOWY,
				RODZAJ,
				--PLU,
				--SWWKU,
				--PKWiU,
				STAN_MINIMALNY,
				STAN_MAKSYMALNY,
				CENA_ZAKUPU_NETTO,
				CENA_ZAKUPU_BRUTTO,
				--ID_CENY_DOM,
				--CERTYFIKAT,
				--DATA_CERTYFIKATU,
				--NAZWA_CERTYFIKATU,
				--OPIS,
				--UWAGI,
				--SEMAFOR,
				--FLAGA_STANU,
				--ID_ARTYKULU_PROD,
				--JEDNOSTKA_PROD,
				--PRZELICZNIK_PROD,
				--ILOSC_PROD,
				NAZWA_ORYG,
				WAGA,
				WYMIAR_W,
				WYMIAR_S,
				WYMIAR_G,
				KOD_KRESKOWY,	
				--WYROZNIK,
				--POLE1,
				--POLE2,
				--POLE3,
				--POLE4,
				--POLE5,
				--POLE6,
				--POLE7,
				--POLE8,
				--POLE9,
				--POLE10,
				JED_WAGI,
				JED_WYMIARU,
				KRAJ_POCHODZENIA,
				--OSTRZEZENIE,        
				--POKAZUJ_OSTRZEZENIE,
				guid_artykul,
				--PODLEGA_RABATOWANIU,
				--KOD_CN,
				--NAZWA_INTRASTAT,
				--ZABLOKOWANY,
				--AKT_CEN_PRZY_DOSTAWIE,
				ID_JEDNOSTKI_REF,
				ID_OPAKOWANIA_REF,
				--PROMOCJA_OD,
				--PROMOCJA_DO,
				--CENA_PROMOCJI_N,
				--CENA_PROMOCJI_B,
				--WYLACZ_CENY_IND,
				--CENA_N_KGO,
				--CENA_B_KGO,
				--PROMOCJA_RABAT,
				PRODUCENT,
				--ID_FPROM,
				--LOKALIZACJA,
				--PRG_LOJAL,
				DATA_ZM_CZ
				--IND_KAT_TOWAR_ZAST,
				--C_ZAKUPU_NETTO_WAL,
				--C_ZAKUPU_BRUTTO_WAL,
				--SYM_WAL,
				--MARZOWY,
				--AKCYZA,
				--AKCYZA_JM,
				--AKCYZA_PRZELICZNIK,
				--AKCYZA_STAWKA_ZA_JM,
				--AKCYZA_PRZELICZNIK_JM_JA,
				--ID_DOSTAWCY_PREFEROWANEGO,
				--ID_PRODUCENTA,
				--DOSTEPNY_W_SKLEPIE_INTER,
				--JEST_ZDJECIE
			)
			select
				(select max(ID_ARTYKULU)+1 from ARTYKUL),
				@id_kategorii,
				@id_kategorii_tree,
				@id_magazynu,
				@id_jednostki,
				0,
				0,
				@VAT_ZAKUPU,
				@VAT_SPRZEDAZY,
				@nazwa,
				'',
				0,
				0,
				0,
				0,
				0,
				@kod_katalogowy,
				@kod_handlowy,
				@RODZAJ,
				--PLU,
				--SWWKU,
				--PKWiU,
				@STAN_MINIMALNY,
				@STAN_MAKSYMALNY,
				0,--cena_zakupu_netto,
				0,--cena_zakupu_brutto,
				--ID_CENY_DOM,
				--CERTYFIKAT,
				--DATA_CERTYFIKATU,
				--NAZWA_CERTYFIKATU,
				--OPIS,
				--UWAGI,
				--SEMAFOR,
				--201,
				--ID_ARTYKULU_PROD,
				--JEDNOSTKA_PROD,
				--PRZELICZNIK_PROD,
				--ILOSC_PROD,
				@nazwa,--NAZWA_ORYG,
				@WAGA,
				@WYMIAR_W,
				@WYMIAR_S,
				@WYMIAR_G,
				@kod_EAN, --KOD_KRESKOWY,
				--WYROZNIK,
				--POLE1,
				--POLE2,
				--POLE3,
				--POLE4,
				--POLE5,
				--POLE6,
				--POLE7,
				--POLE8,
				--POLE9,
				--POLE10,
				@JED_WAGI,
				@JED_WYMIARU,
				@KRAJ_POCHODZENIA,
				--OSTRZEZENIE,        
				--POKAZUJ_OSTRZEZENIE, 
				@guid_artykul,--guid_artykul,
				--PODLEGA_RABATOWANIU,
				--KOD_CN,
				--NAZWA_INTRASTAT,
				--ZABLOKOWANY,
				--AKT_CEN_PRZY_DOSTAWIE,
				0, -- ID_JEDNOSTKI_REF
				0, -- ID_OPAKOWANIA_REF
				--PROMOCJA_OD,
				--PROMOCJA_DO,
				--CENA_PROMOCJI_N,
				--CENA_PROMOCJI_B,
				--WYLACZ_CENY_IND,
				--CENA_N_KGO,
				--CENA_B_KGO,
				--PROMOCJA_RABAT,
				@PRODUCENT,
				--ID_FPROM,
				--LOKALIZACJA,
				--PRG_LOJAL,
				0
				--IND_KAT_TOWAR_ZAST,
				--C_ZAKUPU_NETTO_WAL,
				--C_ZAKUPU_BRUTTO_WAL,
				--SYM_WAL,
				--MARZOWY,
				--AKCYZA,
				--AKCYZA_JM,
				--AKCYZA_PRZELICZNIK,
				--AKCYZA_STAWKA_ZA_JM,
				--AKCYZA_PRZELICZNIK_JM_JA,
				--ID_DOSTAWCY_PREFEROWANEGO,
				--ID_PRODUCENTA,
				--DOSTEPNY_W_SKLEPIE_INTER,
				--JEST_ZDJECIE

	select top 1 @id_artykulu = ID_ARTYKULU
	from ARTYKUL
	where ID_MAGAZYNU = @id_magazynu and guid_artykul = @guid_artykul

	if isnull(@kod_EAN, '') != ''
		insert into KOD_KRESKOWY(ID_MAGAZYNU,KOD_KRESKOWY,ID_ARTYKULU,ID_JEDNOSTKI,OPIS,PRZENOS_OPIS,DOM_ILOSC)
		values(@id_magazynu, @kod_EAN, @id_artykulu, 0, '', 0, 1)

    -- Uzupełnienie danych w artykule o jednostkach domyslnych
	Update ARTYKUL set 
		ID_JEDNOSTKI = @id_jednostki,
		ID_JEDNOSTKI_ZAK = @id_jednostki,
		ID_JEDNOSTKI_SPRZ = @id_jednostki,
		ID_JEDNOSTKI_REF = @id_jednostki,
		POLE1 = '1',
		SEMAFOR = null,
		FLAGA_STANU = 0
	where ID_ARTYKULU = @id_artykulu

--	EXEC AP_WprowadzCenyDlaArt @id_artykulu, @id_kategorii, 1


	insert CENA_ARTYKULU(ID_ARTYKULU, ID_CENY, CENA_NETTO, CENA_BRUTTO, UWAGI, SYM_WAL,OBL_WG_CEN_ZAK,PRZELICZNIK_WAL)
	select @id_artykulu, c.ID_CENY, 0.0000, 0.0000, '', '', 0, 0.0000
	from CENA C
	where C.ID_FIRMY = @id_firmy

	if isnull(@kod_indywidualny, '') != ''
	begin
		insert into  CENNIK_INDYWIDUALNY (ID_ARTYKULU, NAZWA_ARTYKULU, INDEKS, ID_GRUPY, SPRZEDAZ_ZAKUP)
		select TOP 1 @id_artykulu, @nazwa, @kod_indywidualny, K.ID_GRUPY, 0
		from vLokalizacje L
		join KONTRAHENT K on L.idKontrahenta = K.ID_KONTRAHENTA
		where L.lokalizacja = ltrim(rtrim(@part))
		and K.ID_GRUPY is not null
	end

	if isnull(@kod_EAN,'') != '' set @brak_kodu_EAN = 0 else set @brak_kodu_EAN = 1

	if isNull(@kod_indywidualny, '') != '' and exists(select 1
		from vLokalizacje L
		join KONTRAHENT K on L.idKontrahenta = K.ID_KONTRAHENTA
		join CENNIK_INDYWIDUALNY CI on K.ID_GRUPY = CI.ID_GRUPY 
		where L.lokalizacja = ltrim(rtrim(@part))
		and CI.ID_ARTYKULU = @id_artykulu
		and CI.INDEKS = @kod_indywidualny)
	begin
		set @brak_kodu_indywidualnego = 0
	end
	else
		set @brak_kodu_indywidualnego = 1		

	if isnull(@kod_katalogowy,'') != '' set @brak_kodu_katalogowego = 0 else set @brak_kodu_katalogowego = 1

koniec:
  if @@trancount>0  
    commit transaction

  set transaction isolation level READ COMMITTED
  return 0

end
