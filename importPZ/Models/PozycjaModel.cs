﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importPZ.Models
{
    public class PozycjaModel
    {
        public decimal id_artykulu { get; set; }
        /// <summary>
        /// Pola sterujące.
        /// </summary>
        public string STATUS_POZYCJI { get; set; }
        public bool brak_kodu_EAN { get; set; }
        public bool brak_kodu_indywidualnego { get; set; }
        public bool brak_kodu_katalogowego { get; set; }

        public string kod_katalogowy { get; set; } //"kod katalogowy"
        public string kod_handlowy { get; set; } //"kod handlowy"
        public string kod_indywidualny { get; set; } //"kod indywidualny"
        public string kod_EAN { get; set; } //"kod EAN"
        public string nazwa { get; set; } //"nazwa"
        public string jednostka { get; set; } //"jednostka"
        public string pole1_karton { get; set; } //"pole1=karton"
        public string PRODUCENT { get; set; } //"PRODUCENT"
        public string KRAJ_POCHODZENIA { get; set; } //"KRAJ_POCHODZENIA"
        public decimal ilosc_sztuk { get; set; } //"ilość sztuk"
        public decimal cena_netto_domyslna_PLN { get; set; } //"cena netto domyślna PLN"
        public decimal cena_USD { get; set; } //"cena USD"
        public string KATEGORIA { get; set; } //"KATEGORIA"
        public string KATEGORIA_WIELOPOZIOMOWA { get; set; } //"KATEGORIA_WIELOPOZIOMOWA"
        public string JEDNOSTKA_SKROT { get; set; } //"JEDNOSTKA_SKROT"
        public string RODZAJ { get; set; } //"RODZAJ"
        public string VAT_ZAKUPU { get; set; } //"VAT_ZAKUPU"
        public string VAT_SPRZEDAZY { get; set; } //"VAT_SPRZEDAZY"
        public decimal STAN_MINIMALNY { get; set; } //"STAN_MINIMALNY"
        public decimal STAN_MAKSYMALNY { get; set; } //"STAN_MAKSYMALNY"
        public string JED_WAGI { get; set; } //"JED_WAGI"
        public decimal WAGA { get; set; } //"WAGA"
        public string JED_WYMIARU { get; set; } //"JED_WYMIARU"
        public decimal WYMIAR_W { get; set; } //"WYMIAR_W" 
        public decimal WYMIAR_S { get; set; } //"WYMIAR_S"    
        public decimal WYMIAR_G { get; set; } //"WYMIAR_G"
        public string PART { get; set; } //"PART"
        /// <summary>
        /// Pole z widoku vLokalizacja
        /// </summary>
        public long idLokalizacji { get; set; }
    }
}
