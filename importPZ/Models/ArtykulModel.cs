﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importPZ.Models
{
    public class ArtykulModel
    {
        public decimal ID_ARTYKULU { get; set; }
        public string NAZWA { get; set; }
        public string INDEKS_KATALOGOWY { get; set; }
        public string INDEKS_HANDLOWY { get; set; }
        public string KOD_KRESKOWY { get; set; }
        public string KOD_KRESKOWY_DOMYSLNY { get; set; }
    }
}
