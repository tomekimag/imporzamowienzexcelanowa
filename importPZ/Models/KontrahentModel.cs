﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importPZ.Models
{
    public class KontrahentModel
    {
        public decimal ID_KONTRAHENTA { get; set; }
        public Nullable<decimal> ID_FIRMY { get; set; }
        public string NAZWA { get; set; }
        public string SYM_KRAJU { get; set; }
        public Nullable<decimal> ID_PLATNIKA { get; set; }
        public string WOJEWODZTWO { get; set; }
        public string NAZWA_PELNA { get; set; }
    }
}
