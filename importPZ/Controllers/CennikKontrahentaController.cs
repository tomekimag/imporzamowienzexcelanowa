﻿using importPZ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importPZ.Controllers
{
    public static class CennikKontrahentaController
    {
        public static void DopisLubAktualizujIndekKontrahenta(PozycjaModel pozycja)
        {
            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                CENNIK_KONTRAHENTA cennikKontrahenta = (from a in entity.vLokalizacje
                                                          join k in entity.KONTRAHENT on (decimal)(a.idKontrahenta ?? 0) equals k.ID_KONTRAHENTA
                                                          join ck in entity.CENNIK_KONTRAHENTA on k.ID_KONTRAHENTA equals ck.ID_KONTRAHENTA
                                                          where ck.ID_ARTYKULU == pozycja.id_artykulu && a.lokalizacja.Trim().ToUpper() == pozycja.PART.Trim().ToUpper()
                                                          select ck).FirstOrDefault();
                if (cennikKontrahenta != null)
                {
                    cennikKontrahenta.INDEKS = pozycja.kod_indywidualny;
                }
                else
                {
                    KONTRAHENT kontrahent = (from k in entity.KONTRAHENT
                                             join a in entity.vLokalizacje on k.ID_KONTRAHENTA equals (decimal)(a.idKontrahenta ?? 0)
                                             where a.lokalizacja.Trim().ToUpper() == pozycja.PART.Trim().ToUpper()
                                             select k).FirstOrDefault();

                    if (kontrahent != null)
                    {
                        entity.CENNIK_KONTRAHENTA.Add(new CENNIK_KONTRAHENTA()
                        {
                            ID_ARTYKULU = pozycja.id_artykulu,
                            NAZWA_ARTYKULU = pozycja.nazwa,
                            INDEKS = pozycja.kod_indywidualny,
                            ID_KONTRAHENTA = kontrahent.ID_KONTRAHENTA,
                            SPRZEDAZ_ZAKUP = 0,
                            CENA_BRUTTO = 0,
                            CENA_NETTO = 0,
                            SYM_WAL = "",
                            TRYB = 0,
                            UPUST = 0,
                            UWAGI = ""
                        });
                    }
                }

                entity.SaveChanges();
            }
        }
    }
}
