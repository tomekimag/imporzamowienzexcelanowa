﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlTypes;
using importPZ.Models;

namespace importPZ.Controllers
{
    public static class ArtykulController
    {
        public static readonly string connectionString;

        static ArtykulController()
        {
            connectionString = $@"Data Source={ParametryUruchomienioweController.server};Initial Catalog={ParametryUruchomienioweController.bazaDanych};User Id={ParametryUruchomienioweController.uzytkownik};Password={ParametryUruchomienioweController.haslo};";
        }

        public static decimal PobierzIdArtykuluJesliIstnieje(string kod_kreskowy, string kod_indywidualny, string kod_katalogowy, string part,
            ref bool brak_kodu_EAN, ref bool brak_kodu_indywidualnego, ref bool brak_kodu_katalogowego)
        {
            decimal idArtykulu = 0;
            brak_kodu_EAN = true;
            brak_kodu_indywidualnego = true;
            brak_kodu_katalogowego = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("dbo._imag_twit_zwroc_artykul_gdy_istnieje", connection);

                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter id_magazynu = command.Parameters.Add("@id_magazynu", SqlDbType.Decimal);
                    id_magazynu.Direction = ParameterDirection.Input;
                    id_magazynu.Value = ParametryUruchomienioweController.idMagazynu;

                    SqlParameter _kod_kreskowy = command.Parameters.Add("@kod_kreskowy", SqlDbType.VarChar);
                    _kod_kreskowy.Direction = ParameterDirection.Input;
                    _kod_kreskowy.Value = kod_kreskowy;

                    SqlParameter _kod_indywidualny = command.Parameters.Add("@kod_indywidualny", SqlDbType.VarChar);
                    _kod_indywidualny.Direction = ParameterDirection.Input;
                    _kod_indywidualny.Value = kod_indywidualny;

                    SqlParameter _kod_katalogowy = command.Parameters.Add("@kod_katalogowy", SqlDbType.VarChar);
                    _kod_katalogowy.Direction = ParameterDirection.Input;
                    _kod_katalogowy.Value = kod_katalogowy;

                    SqlParameter _part = command.Parameters.Add("@part", SqlDbType.VarChar);
                    _part.Direction = ParameterDirection.Input;
                    _part.Value = part;
                    
                    SqlParameter _id_artykulu = command.Parameters.Add("@id_artykulu", SqlDbType.Decimal);
                    _id_artykulu.Direction = ParameterDirection.Output;

                    SqlParameter _brak_kodu_EAN = command.Parameters.Add("@brak_kodu_EAN", SqlDbType.TinyInt);
                    _brak_kodu_EAN.Direction = ParameterDirection.Output;

                    SqlParameter _brak_kodu_indywidualnego = command.Parameters.Add("@brak_kodu_indywidualnego", SqlDbType.TinyInt);
                    _brak_kodu_indywidualnego.Direction = ParameterDirection.Output;

                    SqlParameter _brak_kodu_katalogowego = command.Parameters.Add("@brak_kodu_katalogowego", SqlDbType.TinyInt);
                    _brak_kodu_katalogowego.Direction = ParameterDirection.Output;

                    var rdr = command.ExecuteNonQuery();

                    idArtykulu = decimal.Parse(_id_artykulu.Value.ToString());
                    brak_kodu_EAN = Int16.Parse(_brak_kodu_EAN.Value.ToString()) == 1 ? true : false;
                    brak_kodu_indywidualnego = Int16.Parse(_brak_kodu_indywidualnego.Value.ToString()) == 1 ? true : false;
                    brak_kodu_katalogowego = Int16.Parse(_brak_kodu_katalogowego.Value.ToString()) == 1 ? true : false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return idArtykulu;
        }

        public static decimal UtworzArtykulIZwrocIdArtykulu(PozycjaModel pozycja)
        {
            decimal idArtykulu = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("dbo._imag_twit_zakladanie_artykulu", connection);

                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter id_magazynu = command.Parameters.Add("@id_magazynu", SqlDbType.Decimal);
                    id_magazynu.Direction = ParameterDirection.Input;
                    id_magazynu.Value = ParametryUruchomienioweController.idMagazynu;

                    SqlParameter KATEGORIA = command.Parameters.Add("@KATEGORIA", SqlDbType.VarChar);
                    KATEGORIA.Direction = ParameterDirection.Input;
                    KATEGORIA.Value = pozycja.KATEGORIA;

                    SqlParameter KATEGORIA_WIELOPOZIOMOWA = command.Parameters.Add("@KATEGORIA_WIELOPOZIOMOWA", SqlDbType.VarChar);
                    KATEGORIA_WIELOPOZIOMOWA.Direction = ParameterDirection.Input;
                    KATEGORIA_WIELOPOZIOMOWA.Value = pozycja.KATEGORIA_WIELOPOZIOMOWA;

                    SqlParameter JEDNOSTKA_SKROT = command.Parameters.Add("@JEDNOSTKA_SKROT", SqlDbType.VarChar);
                    JEDNOSTKA_SKROT.Direction = ParameterDirection.Input;
                    JEDNOSTKA_SKROT.Value = pozycja.JEDNOSTKA_SKROT;

                    SqlParameter VAT_ZAKUPU = command.Parameters.Add("@VAT_ZAKUPU", SqlDbType.Char, 3);
                    VAT_ZAKUPU.Direction = ParameterDirection.Input;
                    VAT_ZAKUPU.Value = pozycja.VAT_ZAKUPU;

                    SqlParameter VAT_SPRZEDAZY = command.Parameters.Add("@VAT_SPRZEDAZY", SqlDbType.Char, 3);
                    VAT_SPRZEDAZY.Direction = ParameterDirection.Input;
                    VAT_SPRZEDAZY.Value = pozycja.VAT_SPRZEDAZY;

                    SqlParameter nazwa = command.Parameters.Add("@nazwa", SqlDbType.VarChar);
                    nazwa.Direction = ParameterDirection.Input;
                    nazwa.Value = pozycja.nazwa;

                    SqlParameter kod_katalogowy = command.Parameters.Add("@kod_katalogowy", SqlDbType.VarChar);
                    kod_katalogowy.Direction = ParameterDirection.Input;
                    kod_katalogowy.Value = pozycja.kod_katalogowy ?? "";

                    SqlParameter kod_handlowy = command.Parameters.Add("@kod_handlowy", SqlDbType.VarChar);
                    kod_handlowy.Direction = ParameterDirection.Input;
                    kod_handlowy.Value = pozycja.kod_handlowy ?? "";

                    SqlParameter RODZAJ = command.Parameters.Add("@RODZAJ", SqlDbType.VarChar);
                    RODZAJ.Direction = ParameterDirection.Input;
                    RODZAJ.Value = pozycja.RODZAJ ?? "";

                    SqlParameter STAN_MINIMALNY = command.Parameters.Add("@STAN_MINIMALNY", SqlDbType.Decimal);
                    STAN_MINIMALNY.Direction = ParameterDirection.Input;
                    STAN_MINIMALNY.Value = pozycja.STAN_MINIMALNY;

                    SqlParameter STAN_MAKSYMALNY = command.Parameters.Add("@STAN_MAKSYMALNY", SqlDbType.Decimal);
                    STAN_MAKSYMALNY.Direction = ParameterDirection.Input;
                    STAN_MAKSYMALNY.Value = pozycja.STAN_MAKSYMALNY;

                    SqlParameter WAGA = command.Parameters.Add("@WAGA", SqlDbType.Decimal);
                    WAGA.Direction = ParameterDirection.Input;
                    WAGA.Value = pozycja.WAGA;

                    SqlParameter JED_WAGI = command.Parameters.Add("@JED_WAGI", SqlDbType.VarChar);
                    JED_WAGI.Direction = ParameterDirection.Input;
                    JED_WAGI.Value = pozycja.JED_WAGI;

                    SqlParameter JED_WYMIARU = command.Parameters.Add("@JED_WYMIARU", SqlDbType.VarChar);
                    JED_WYMIARU.Direction = ParameterDirection.Input;
                    JED_WYMIARU.Value = pozycja.JED_WYMIARU;

                    SqlParameter WYMIAR_W = command.Parameters.Add("@WYMIAR_W", SqlDbType.Decimal);
                    WYMIAR_W.Direction = ParameterDirection.Input;
                    WYMIAR_W.Value = pozycja.WYMIAR_W;


                    SqlParameter WYMIAR_S = command.Parameters.Add("@WYMIAR_S", SqlDbType.Decimal);
                    WYMIAR_S.Direction = ParameterDirection.Input;
                    WYMIAR_S.Value = pozycja.WYMIAR_S;

                    SqlParameter WYMIAR_G = command.Parameters.Add("@WYMIAR_G", SqlDbType.Decimal);
                    WYMIAR_G.Direction = ParameterDirection.Input;
                    WYMIAR_G.Value = pozycja.WYMIAR_G;

                    SqlParameter kod_EAN = command.Parameters.Add("@kod_EAN", SqlDbType.VarChar);
                    kod_EAN.Direction = ParameterDirection.Input;
                    kod_EAN.Value = pozycja.kod_EAN ?? "";

                    SqlParameter KRAJ_POCHODZENIA = command.Parameters.Add("@KRAJ_POCHODZENIA", SqlDbType.VarChar);
                    KRAJ_POCHODZENIA.Direction = ParameterDirection.Input;
                    KRAJ_POCHODZENIA.Value = pozycja.KRAJ_POCHODZENIA;

                    SqlParameter PRODUCENT = command.Parameters.Add("@PRODUCENT", SqlDbType.VarChar);
                    PRODUCENT.Direction = ParameterDirection.Input;
                    PRODUCENT.Value = pozycja.PRODUCENT ?? "";

                    SqlParameter kod_indywidualny = command.Parameters.Add("@kod_indywidualny", SqlDbType.VarChar);
                    kod_indywidualny.Direction = ParameterDirection.Input;
                    kod_indywidualny.Value = pozycja.kod_indywidualny ?? "";

                    SqlParameter part = command.Parameters.Add("@part", SqlDbType.VarChar);
                    part.Direction = ParameterDirection.Input;
                    part.Value = pozycja.PART;

                    SqlParameter _id_artykulu = command.Parameters.Add("@id_artykulu", SqlDbType.Decimal);
                    _id_artykulu.Direction = ParameterDirection.Output;

                    SqlParameter _brak_kodu_EAN = command.Parameters.Add("@brak_kodu_EAN", SqlDbType.TinyInt);
                    _brak_kodu_EAN.Direction = ParameterDirection.Output;

                    SqlParameter _brak_kodu_indywidualnego = command.Parameters.Add("@brak_kodu_indywidualnego", SqlDbType.TinyInt);
                    _brak_kodu_indywidualnego.Direction = ParameterDirection.Output;

                    SqlParameter _brak_kodu_katalogowego = command.Parameters.Add("@brak_kodu_katalogowego", SqlDbType.TinyInt);
                    _brak_kodu_katalogowego.Direction = ParameterDirection.Output;

                    var rdr = command.ExecuteNonQuery();

                    idArtykulu = decimal.Parse(_id_artykulu.Value.ToString());
                    pozycja.brak_kodu_EAN = Int16.Parse(_brak_kodu_EAN.Value.ToString()) == 1 ? true : false;
                    pozycja.brak_kodu_indywidualnego = Int16.Parse(_brak_kodu_indywidualnego.Value.ToString()) == 1 ? true : false;
                    pozycja.brak_kodu_katalogowego = Int16.Parse(_brak_kodu_katalogowego.Value.ToString()) == 1 ? true : false;

                    idArtykulu = decimal.Parse(_id_artykulu.Value.ToString());
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return idArtykulu;
        }

        public static List<ArtykulModel> PobierzArtykuly()
        {
            List<ArtykulModel> artykuly;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                artykuly = (from a in entity.ARTYKUL
                            join kk in entity.KOD_KRESKOWY on a.ID_ARTYKULU equals kk.ID_ARTYKULU
                            where a.ID_MAGAZYNU == ParametryUruchomienioweController.idMagazynu
                            select new ArtykulModel()
                            {
                                ID_ARTYKULU = a.ID_ARTYKULU,
                                INDEKS_HANDLOWY = a.INDEKS_HANDLOWY,
                                INDEKS_KATALOGOWY = a.INDEKS_KATALOGOWY,
                                KOD_KRESKOWY_DOMYSLNY = a.KOD_KRESKOWY,
                                KOD_KRESKOWY = kk.KOD_KRESKOWY1,
                                NAZWA = a.NAZWA
                            }).ToList();
            }

            return artykuly;
        }

        public static bool DodajKodKresowy(decimal id_artykulu, string kod_kreskowy, bool czyDomyslnyKodKreskowy)
        {
            bool result = false;
            try
            {
                using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
                {
                    entity.KOD_KRESKOWY.Add(new KOD_KRESKOWY()
                    {
                        ID_MAGAZYNU = ParametryUruchomienioweController.idMagazynu,
                        ID_ARTYKULU = id_artykulu,
                        KOD_KRESKOWY1 = kod_kreskowy,
                        ID_JEDNOSTKI = 0,
                        DOM_ILOSC = 1,
                        OPIS = "",
                        OPIS_CD = "",
                        PRZENOS_OPIS = 0
                    });

                    if (czyDomyslnyKodKreskowy)
                    {
                        ARTYKUL artykul = (from a in entity.ARTYKUL
                                           where a.ID_ARTYKULU == id_artykulu
                                           select a).FirstOrDefault();

                        if (artykul != null)
                        {
                            artykul.KOD_KRESKOWY = kod_kreskowy;
                        }
                    }

                    entity.SaveChanges();

                    result = true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show($@"Błąd dodania kodu kreskowego: {e.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return result;
        }

        public static int ZaktualizujArtykuly()
        {
            int wynik = -1;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("dbo._imag_twit_aktualizacja_artykulu", connection);

                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter id_uzytkownika = command.Parameters.Add("@id_uzytkownika", SqlDbType.Decimal);
                    id_uzytkownika.Direction = ParameterDirection.Input;
                    id_uzytkownika.Value = ParametryUruchomienioweController.idUzytkownika;

                    SqlParameter _wynik = command.Parameters.Add("@wynik", SqlDbType.Int);
                    _wynik.Direction = ParameterDirection.Output;

                    var rdr = command.ExecuteNonQuery();

                    wynik = int.Parse(_wynik.Value.ToString());
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                wynik = -1;
            }
            return wynik;
        }
    }
}
