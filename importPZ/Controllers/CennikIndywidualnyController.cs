﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importPZ.Models;

namespace importPZ.Controllers
{
    public static class CennikIndywidualnyController
    {
        public static void DopisLubAktualizujIndekIndywidualny(PozycjaModel pozycja)
        {
            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                CENNIK_INDYWIDUALNY cennikIndywidualny = (from a in entity.vLokalizacje
                                                          join k in entity.KONTRAHENT on (decimal)(a.idKontrahenta ?? 0) equals k.ID_KONTRAHENTA
                                                          join ci in entity.CENNIK_INDYWIDUALNY on k.ID_GRUPY equals ci.ID_GRUPY
                                                          where ci.ID_ARTYKULU == pozycja.id_artykulu && a.lokalizacja.Trim().ToUpper() == pozycja.PART.Trim().ToUpper()
                                                          select ci).FirstOrDefault();
                if (cennikIndywidualny != null)
                {
                    cennikIndywidualny.INDEKS = pozycja.kod_indywidualny;
                }
                else
                {
                    KONTRAHENT kontrahent = (from k in entity.KONTRAHENT
                                             join a in entity.vLokalizacje on k.ID_KONTRAHENTA equals (decimal)(a.idKontrahenta ?? 0)
                                             where a.lokalizacja.Trim().ToUpper() == pozycja.PART.Trim().ToUpper()
                                             select k).FirstOrDefault();

                    if ((kontrahent != null) && kontrahent.ID_GRUPY != null)
                    {
                        entity.CENNIK_INDYWIDUALNY.Add(new CENNIK_INDYWIDUALNY() {
                            ID_ARTYKULU = pozycja.id_artykulu,
                            NAZWA_ARTYKULU = pozycja.nazwa,
                            INDEKS = pozycja.kod_indywidualny,
                            ID_GRUPY = kontrahent.ID_GRUPY??0,
                            SPRZEDAZ_ZAKUP = 0
                        });
                    }
                }

                entity.SaveChanges();
            }
        }
    }
}
