﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importPZ.Models;

namespace importPZ.Controllers
{
    public static class vLokalizacjeController
    {
        public static List<vLokalizacjeModel> PobierzLokalizacje()
        {
            List<vLokalizacjeModel> lokalizacje;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                lokalizacje = (from a in entity.vLokalizacje
                               select new vLokalizacjeModel()
                               {
                                   idKontrahenta = a.idKontrahenta,
                                   idLokalizacji = a.idLokalizacji,
                                   lokalizacja = a.lokalizacja
                               }).ToList();
            }

            return lokalizacje;
        }
    }
}
