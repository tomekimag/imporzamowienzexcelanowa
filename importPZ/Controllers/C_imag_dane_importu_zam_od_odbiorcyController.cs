﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importPZ.Models;
using System.Windows.Forms;

namespace importPZ.Controllers
{
    public static class C_imag_dane_importu_zam_od_odbiorcyController
    {
        public static readonly string connectionString;            

        static C_imag_dane_importu_zam_od_odbiorcyController()
        {
            connectionString = $@"Data Source={ParametryUruchomienioweController.server};Initial Catalog={ParametryUruchomienioweController.bazaDanych};User Id={ParametryUruchomienioweController.uzytkownik};Password={ParametryUruchomienioweController.haslo};";
        }

        public static void UsunStareDaneImportowe()
        {
            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                var listaDanychImportowych = from di in entity.C_imag_dane_importu_zam_od_odbiorcy
                                              where di.id_uzytkownika == ParametryUruchomienioweController.idUzytkownika
                                              select di;

                if ((listaDanychImportowych != null) && (listaDanychImportowych.Count() != 0))
                {
                    entity.C_imag_dane_importu_zam_od_odbiorcy.RemoveRange(listaDanychImportowych);

                    entity.SaveChanges();
                }
            }
        }

        public static void WypelnijDaneImportowe(List<PozycjaModel> pozycje)
        {
            try
            {
                //MessageBox.Show($@"pozycje = {pozycje.Count()}");

                using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
                {
                    foreach (PozycjaModel pozycja in pozycje)
                    {
                        string nazwa = string.Empty;
                        if (pozycja.nazwa != null)
                        {
                            if (pozycja.nazwa.Length > 40) { nazwa = pozycja.nazwa.Substring(1, 39); } else { nazwa = pozycja.nazwa; }
                        }
                        else
                            nazwa = "";
                        //MessageBox.Show($@"pozycja = {pozycja.kod_katalogowy}");

                        entity.C_imag_dane_importu_zam_od_odbiorcy.Add(new C_imag_dane_importu_zam_od_odbiorcy() {
                            id_uzytkownika = ParametryUruchomienioweController.idUzytkownika,
                            cena_netto_domyslna_PLN = pozycja.cena_netto_domyslna_PLN,
                            cena_USD = pozycja.cena_USD,
                            id_artykulu = pozycja.id_artykulu,
                            ilosc_sztuk = pozycja.ilosc_sztuk,
                            jednostka = pozycja.jednostka,
                            JEDNOSTKA_SKROT = pozycja.JEDNOSTKA_SKROT,
                            JED_WAGI = pozycja.JED_WAGI??"",
                            JED_WYMIARU = pozycja.JED_WYMIARU??"",
                            KATEGORIA = (pozycja.KATEGORIA ?? ""),
                            KATEGORIA_WIELOPOZIOMOWA = (pozycja.KATEGORIA_WIELOPOZIOMOWA ?? ""),
                            kod_EAN = (pozycja.kod_EAN??"").Trim(),
                            kod_handlowy = (pozycja.kod_handlowy??"").Trim(),
                            kod_katalogowy = (pozycja.kod_katalogowy??"").Trim(),
                            KRAJ_POCHODZENIA = pozycja.KRAJ_POCHODZENIA??"",
                            nazwa = nazwa,
                            PART = pozycja.PART,
                            pole1_karton = pozycja.pole1_karton??"",
                            PRODUCENT = pozycja.PRODUCENT??"",
                            RODZAJ = pozycja.RODZAJ??"",
                            STAN_MAKSYMALNY = pozycja.STAN_MAKSYMALNY,
                            STAN_MINIMALNY = pozycja.STAN_MINIMALNY,
                            VAT_SPRZEDAZY = pozycja.VAT_SPRZEDAZY??"",
                            VAT_ZAKUPU = pozycja.VAT_ZAKUPU??"",
                            WAGA = pozycja.WAGA,
                            WYMIAR_G = pozycja.WYMIAR_G,
                            WYMIAR_S = pozycja.WYMIAR_S,
                            WYMIAR_W = pozycja.WYMIAR_W,
                            idLokalizacji = pozycja.idLokalizacji
                        });
                        entity.SaveChanges();
                    }
                    
                }
            }
            catch (Exception e)
            {
                MessageBox.Show($@"Błąd zapisu danych importowych w bazie: {e.Message} {e.InnerException.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
