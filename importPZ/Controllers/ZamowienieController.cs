﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlTypes;
using importPZ.Models;


namespace importPZ.Controllers
{
    public static class ZamowienieController
    {
        public static readonly string connectionString;

        static ZamowienieController()
        {
            connectionString = $@"Data Source={ParametryUruchomienioweController.server};Initial Catalog={ParametryUruchomienioweController.bazaDanych};User Id={ParametryUruchomienioweController.uzytkownik};Password={ParametryUruchomienioweController.haslo};";
        }

        public static string UtworzZamowienie(decimal idKontrahenta, DateTime dataDokumentu)
        {
            string nrZamowienia = string.Empty;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("dbo.IMAG_twit_dodaj_zamowienia_do_dostawcow", connection);

                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter IdFirmy = command.Parameters.Add("@IdFirmy", SqlDbType.Decimal);
                    IdFirmy.Direction = ParameterDirection.Input;
                    IdFirmy.Value = ParametryUruchomienioweController.idFirmy;

                    SqlParameter IdMagazynu = command.Parameters.Add("@IdMagazynu", SqlDbType.Decimal);
                    IdMagazynu.Direction = ParameterDirection.Input;
                    IdMagazynu.Value = ParametryUruchomienioweController.idMagazynu;

                    SqlParameter IdUzytkownika = command.Parameters.Add("@IdUzytkownika", SqlDbType.Decimal);
                    IdUzytkownika.Direction = ParameterDirection.Input;
                    IdUzytkownika.Value = ParametryUruchomienioweController.idUzytkownika;

                    SqlParameter IdKontrahenta = command.Parameters.Add("@IdKontrahenta", SqlDbType.Decimal);
                    IdKontrahenta.Direction = ParameterDirection.Input;
                    IdKontrahenta.Value = idKontrahenta;
                    

                    SqlParameter _dataDokumentu = command.Parameters.Add("@dataDokumentu", SqlDbType.DateTime);
                    _dataDokumentu.Direction = ParameterDirection.Input;
                    _dataDokumentu.Value = dataDokumentu;

                    SqlParameter numerZamowienia = command.Parameters.Add("@numerZamowienia", SqlDbType.VarChar, 30);
                    numerZamowienia.Direction = ParameterDirection.Output;

                    var rdr = command.ExecuteNonQuery();

                    nrZamowienia = numerZamowienia.Value.ToString();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return nrZamowienia;
        }
    }
}
