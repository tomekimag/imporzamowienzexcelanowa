﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importPZ.Models;

namespace importPZ.Controllers
{
    public class KontrahentController
    {
        private NOWASA2018Entities entity;

        public KontrahentController(NOWASA2018Entities entity)
        {
            this.entity = entity;
        }

        public List<KontrahentModel> pobierzKontrahentow(decimal idFirmy)
        {
            List<KontrahentModel> listaKontrahentow = (from k in entity.KONTRAHENT
                                                       where k.ID_FIRMY == idFirmy
                                                       select new KontrahentModel() {
                                                           ID_FIRMY = k.ID_FIRMY,
                                                           ID_KONTRAHENTA = k.ID_KONTRAHENTA,
                                                           ID_PLATNIKA = k.ID_PLATNIKA,
                                                           NAZWA = k.NAZWA,
                                                           NAZWA_PELNA = k.NAZWA_PELNA,
                                                           SYM_KRAJU = k.SYM_KRAJU,
                                                           WOJEWODZTWO = k.WOJEWODZTWO
                                                       }).ToList();

            return listaKontrahentow;
        }
    }
}
