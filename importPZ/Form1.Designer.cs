﻿namespace importPZ
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_sciezkaXLSX = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker_dataDokumentu = new System.Windows.Forms.DateTimePicker();
            this.button_listaKontrahentow = new System.Windows.Forms.Button();
            this.textBox_kontrahent = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_dopiszKodIndywidualny = new System.Windows.Forms.Button();
            this.button_dopiszKodEAN = new System.Windows.Forms.Button();
            this.button_utworzArtykul = new System.Windows.Forms.Button();
            this.button_powiaz = new System.Windows.Forms.Button();
            this.button_import = new System.Windows.Forms.Button();
            this.button_zamknij = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView_pozycje = new System.Windows.Forms.DataGridView();
            this.button_dodajKodKontrahenta = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_pozycje)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBox_sciezkaXLSX);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dateTimePicker_dataDokumentu);
            this.panel1.Controls.Add(this.button_listaKontrahentow);
            this.panel1.Controls.Add(this.textBox_kontrahent);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1188, 100);
            this.panel1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Plik excel:";
            // 
            // textBox_sciezkaXLSX
            // 
            this.textBox_sciezkaXLSX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_sciezkaXLSX.Enabled = false;
            this.textBox_sciezkaXLSX.Location = new System.Drawing.Point(83, 66);
            this.textBox_sciezkaXLSX.Name = "textBox_sciezkaXLSX";
            this.textBox_sciezkaXLSX.Size = new System.Drawing.Size(890, 20);
            this.textBox_sciezkaXLSX.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(979, 64);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(197, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Wczytaj plik do zaimportowania";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker_dataDokumentu
            // 
            this.dateTimePicker_dataDokumentu.Location = new System.Drawing.Point(83, 38);
            this.dateTimePicker_dataDokumentu.Name = "dateTimePicker_dataDokumentu";
            this.dateTimePicker_dataDokumentu.Size = new System.Drawing.Size(213, 20);
            this.dateTimePicker_dataDokumentu.TabIndex = 5;
            // 
            // button_listaKontrahentow
            // 
            this.button_listaKontrahentow.Location = new System.Drawing.Point(502, 10);
            this.button_listaKontrahentow.Name = "button_listaKontrahentow";
            this.button_listaKontrahentow.Size = new System.Drawing.Size(29, 23);
            this.button_listaKontrahentow.TabIndex = 4;
            this.button_listaKontrahentow.Text = "...";
            this.button_listaKontrahentow.UseVisualStyleBackColor = true;
            this.button_listaKontrahentow.Click += new System.EventHandler(this.button_listaKontrahentow_Click);
            // 
            // textBox_kontrahent
            // 
            this.textBox_kontrahent.Enabled = false;
            this.textBox_kontrahent.Location = new System.Drawing.Point(83, 12);
            this.textBox_kontrahent.Name = "textBox_kontrahent";
            this.textBox_kontrahent.Size = new System.Drawing.Size(413, 20);
            this.textBox_kontrahent.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kontrahent:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_dodajKodKontrahenta);
            this.panel2.Controls.Add(this.button_dopiszKodIndywidualny);
            this.panel2.Controls.Add(this.button_dopiszKodEAN);
            this.panel2.Controls.Add(this.button_utworzArtykul);
            this.panel2.Controls.Add(this.button_powiaz);
            this.panel2.Controls.Add(this.button_import);
            this.panel2.Controls.Add(this.button_zamknij);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 443);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1188, 73);
            this.panel2.TabIndex = 1;
            // 
            // button_dopiszKodIndywidualny
            // 
            this.button_dopiszKodIndywidualny.Location = new System.Drawing.Point(548, 22);
            this.button_dopiszKodIndywidualny.Name = "button_dopiszKodIndywidualny";
            this.button_dopiszKodIndywidualny.Size = new System.Drawing.Size(188, 23);
            this.button_dopiszKodIndywidualny.TabIndex = 5;
            this.button_dopiszKodIndywidualny.Text = "Dopisz kod indywidualny";
            this.button_dopiszKodIndywidualny.UseVisualStyleBackColor = true;
            this.button_dopiszKodIndywidualny.Click += new System.EventHandler(this.button_dopiszKodIndywidualny_Click);
            // 
            // button_dopiszKodEAN
            // 
            this.button_dopiszKodEAN.Location = new System.Drawing.Point(388, 22);
            this.button_dopiszKodEAN.Name = "button_dopiszKodEAN";
            this.button_dopiszKodEAN.Size = new System.Drawing.Size(154, 23);
            this.button_dopiszKodEAN.TabIndex = 4;
            this.button_dopiszKodEAN.Text = "Dopisz kod EAN";
            this.button_dopiszKodEAN.UseVisualStyleBackColor = true;
            this.button_dopiszKodEAN.Click += new System.EventHandler(this.button_dopiszKodEAN_Click);
            // 
            // button_utworzArtykul
            // 
            this.button_utworzArtykul.Location = new System.Drawing.Point(228, 22);
            this.button_utworzArtykul.Name = "button_utworzArtykul";
            this.button_utworzArtykul.Size = new System.Drawing.Size(154, 23);
            this.button_utworzArtykul.TabIndex = 3;
            this.button_utworzArtykul.Text = "Utwórz nowy artykuł";
            this.button_utworzArtykul.UseVisualStyleBackColor = true;
            this.button_utworzArtykul.Click += new System.EventHandler(this.button_utworzArtykul_Click);
            // 
            // button_powiaz
            // 
            this.button_powiaz.Location = new System.Drawing.Point(25, 22);
            this.button_powiaz.Name = "button_powiaz";
            this.button_powiaz.Size = new System.Drawing.Size(197, 23);
            this.button_powiaz.TabIndex = 2;
            this.button_powiaz.Text = "Wybierz artykuł do powiązania";
            this.button_powiaz.UseVisualStyleBackColor = true;
            this.button_powiaz.Click += new System.EventHandler(this.button_powiaz_Click);
            // 
            // button_import
            // 
            this.button_import.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_import.Enabled = false;
            this.button_import.Location = new System.Drawing.Point(1005, 22);
            this.button_import.Name = "button_import";
            this.button_import.Size = new System.Drawing.Size(75, 23);
            this.button_import.TabIndex = 1;
            this.button_import.Text = "Import";
            this.button_import.UseVisualStyleBackColor = true;
            this.button_import.Click += new System.EventHandler(this.button_import_Click);
            // 
            // button_zamknij
            // 
            this.button_zamknij.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zamknij.Location = new System.Drawing.Point(1086, 22);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(75, 23);
            this.button_zamknij.TabIndex = 0;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = true;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView_pozycje);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 100);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1188, 343);
            this.panel3.TabIndex = 1;
            // 
            // dataGridView_pozycje
            // 
            this.dataGridView_pozycje.AllowUserToAddRows = false;
            this.dataGridView_pozycje.AllowUserToDeleteRows = false;
            this.dataGridView_pozycje.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_pozycje.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_pozycje.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_pozycje.Name = "dataGridView_pozycje";
            this.dataGridView_pozycje.ReadOnly = true;
            this.dataGridView_pozycje.Size = new System.Drawing.Size(1188, 343);
            this.dataGridView_pozycje.TabIndex = 0;
            this.dataGridView_pozycje.DataSourceChanged += new System.EventHandler(this.dataGridView_pozycje_DataSourceChanged);
            this.dataGridView_pozycje.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_pozycje_ColumnAdded);
            this.dataGridView_pozycje.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_pozycje_RowEnter);
            // 
            // button_dodajKodKontrahenta
            // 
            this.button_dodajKodKontrahenta.Location = new System.Drawing.Point(742, 22);
            this.button_dodajKodKontrahenta.Name = "button_dodajKodKontrahenta";
            this.button_dodajKodKontrahenta.Size = new System.Drawing.Size(174, 23);
            this.button_dodajKodKontrahenta.TabIndex = 6;
            this.button_dodajKodKontrahenta.Text = "Dopisz kod kontrahenta";
            this.button_dodajKodKontrahenta.UseVisualStyleBackColor = true;
            this.button_dodajKodKontrahenta.Click += new System.EventHandler(this.button_dodajKodKontrahenta_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1188, 516);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Import zamówienia do dostawcy";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_pozycje)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox_kontrahent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_listaKontrahentow;
        private System.Windows.Forms.DateTimePicker dateTimePicker_dataDokumentu;
        private System.Windows.Forms.DataGridView dataGridView_pozycje;
        private System.Windows.Forms.Button button_import;
        private System.Windows.Forms.Button button_zamknij;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_sciezkaXLSX;
        private System.Windows.Forms.Button button_powiaz;
        private System.Windows.Forms.Button button_utworzArtykul;
        private System.Windows.Forms.Button button_dopiszKodIndywidualny;
        private System.Windows.Forms.Button button_dopiszKodEAN;
        private System.Windows.Forms.Button button_dodajKodKontrahenta;
    }
}

