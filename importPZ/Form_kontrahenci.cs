﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using importPZ.Models;
using importPZ.Controllers;

namespace importPZ
{
    public partial class Form_kontrahenci : Form
    {
        private List<KontrahentModel> listaKontrahentow;

        public KontrahentModel wybranyKontrahent;

        public Form_kontrahenci()
        {
            InitializeComponent();
            ZaladujKontrahentow();
        }

        private void ZaladujKontrahentow()
        {
            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                KontrahentController kontrahentController = new KontrahentController(entity);
                listaKontrahentow = kontrahentController.pobierzKontrahentow(ParametryUruchomienioweController.idFirmy);
                dataGridView_kontrahenci.DataSource = listaKontrahentow;
            }
        }

        private void button_anuluj_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_wybierz_Click(object sender, EventArgs e)
        {
            if (dataGridView_kontrahenci.SelectedRows == null)
            {
                MessageBox.Show("Wskarz kontrahenta.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_kontrahenci.SelectedRows.Count != 0)
            {
                wybranyKontrahent = new KontrahentModel()
                {
                    ID_KONTRAHENTA = decimal.Parse(dataGridView_kontrahenci["ID_KONTRAHENTA", dataGridView_kontrahenci.SelectedRows[0].Index].Value.ToString()),
                    NAZWA = dataGridView_kontrahenci["NAZWA", dataGridView_kontrahenci.SelectedRows[0].Index].Value.ToString()
                };
                if (wybranyKontrahent != null)
                    this.DialogResult = DialogResult.Yes;
            }
        }

        private void dataGridView_kontrahenci_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void dataGridView_kontrahenci_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "NAZWA":
                    e.Column.Visible = true;
                    break;
                case "SYM_KRAJU":
                    e.Column.Visible = true;
                    break;
                case "WOJEWODZTWO":
                    e.Column.Visible = true;
                    break;
                case "NAZWA_PELNA":
                    e.Column.Visible = true;
                    break;
                default:
                    e.Column.Visible = false;
                    break;
            }
        }

        private void button_szukaj_Click(object sender, EventArgs e)
        {
            dataGridView_kontrahenci.DataSource = listaKontrahentow.Where(x=> x.NAZWA.ToUpper().Contains(textBox_ciagNazwy.Text.ToUpper())).ToList();
        }

        private void button_wszystko_Click(object sender, EventArgs e)
        {
            dataGridView_kontrahenci.DataSource = listaKontrahentow;
        }
    }
}
