﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LinqToExcel;
using importPZ.Models;
using importPZ.Controllers;

namespace importPZ
{
    public partial class Form1 : Form
    {
        private KontrahentModel wybranyKontrahent;

        private List<PozycjaModel> pozycje;

        public Form1()
        {
            InitializeComponent();
            UstawienieBierzacejDatyDlaDokumentu();
        }

        private void UstawienieBierzacejDatyDlaDokumentu()
        {
            dateTimePicker_dataDokumentu.Value = DateTime.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox_sciezkaXLSX.Text = openFileDialog1.FileName;
                WczytajPlikExcel();
                SprawdzStatusArtykulow();
                dataGridView_pozycje.DataSource = pozycje;
            }
        }

        private void WczytajPlikExcel()
        {
            try
            {
                var excel = new ExcelQueryFactory(textBox_sciezkaXLSX.Text);

                excel.AddMapping<PozycjaModel>(x => x.kod_katalogowy, "kod katalogowy");
                excel.AddMapping<PozycjaModel>(x => x.kod_handlowy, "kod handlowy");
                excel.AddMapping<PozycjaModel>(x => x.kod_indywidualny, "kod indywidualny");
                excel.AddMapping<PozycjaModel>(x => x.kod_EAN, "kod EAN");
                excel.AddMapping<PozycjaModel>(x => x.nazwa, "nazwa");
                excel.AddMapping<PozycjaModel>(x => x.jednostka, "jednostka");
                excel.AddMapping<PozycjaModel>(x => x.pole1_karton, "pole1=karton");
                excel.AddMapping<PozycjaModel>(x => x.PRODUCENT, "PRODUCENT");
                excel.AddMapping<PozycjaModel>(x => x.KRAJ_POCHODZENIA, "KRAJ_POCHODZENIA");
                excel.AddMapping<PozycjaModel>(x => x.ilosc_sztuk, "ilość sztuk");
                excel.AddMapping<PozycjaModel>(x => x.cena_netto_domyslna_PLN, "cena netto domyślna PLN");
                excel.AddMapping<PozycjaModel>(x => x.cena_USD, "cena USD");
                excel.AddMapping<PozycjaModel>(x => x.KATEGORIA, "KATEGORIA");
                excel.AddMapping<PozycjaModel>(x => x.KATEGORIA_WIELOPOZIOMOWA, "KATEGORIA_WIELOPOZIOMOWA");
                excel.AddMapping<PozycjaModel>(x => x.JEDNOSTKA_SKROT, "JEDNOSTKA_SKROT");
                excel.AddMapping<PozycjaModel>(x => x.RODZAJ, "RODZAJ");
                excel.AddMapping<PozycjaModel>(x => x.VAT_ZAKUPU, "VAT_ZAKUPU");
                excel.AddMapping<PozycjaModel>(x => x.VAT_SPRZEDAZY, "VAT_SPRZEDAZY");
                excel.AddMapping<PozycjaModel>(x => x.STAN_MINIMALNY, "STAN_MINIMALNY");
                excel.AddMapping<PozycjaModel>(x => x.STAN_MAKSYMALNY, "STAN_MAKSYMALNY");
                excel.AddMapping<PozycjaModel>(x => x.JED_WAGI, "JED_WAGI");
                excel.AddMapping<PozycjaModel>(x => x.WAGA, "WAGA");
                excel.AddMapping<PozycjaModel>(x => x.JED_WYMIARU, "JED_WYMIARU");
                excel.AddMapping<PozycjaModel>(x => x.WYMIAR_W, "WYMIAR_W");
                excel.AddMapping<PozycjaModel>(x => x.WYMIAR_S, "WYMIAR_S");
                excel.AddMapping<PozycjaModel>(x => x.WYMIAR_G, "WYMIAR_G");
                excel.AddMapping<PozycjaModel>(x => x.PART, "PART");

                pozycje = (from c in excel.Worksheet<PozycjaModel>("Arkusz1")
                           select c).ToList()
                           .Where(c => (c.kod_EAN != "" || c.kod_katalogowy != "" || c.kod_indywidualny != "") && c.PART != "" && c.ilosc_sztuk != 0)
                           .ToList();

                List<vLokalizacjeModel> lokalizacje = vLokalizacjeController.PobierzLokalizacje();

                foreach (PozycjaModel pozycja in pozycje) {
                    pozycja.STATUS_POZYCJI = "Nie rozpoznany";

                    vLokalizacjeModel lokalizacja = lokalizacje.Where(x => x.lokalizacja.ToUpper() == pozycja.PART.ToUpper()).FirstOrDefault();
                        
                    if (lokalizacja == null)
                        MessageBox.Show($@"Brak lokalizacji dla {pozycja.PART}.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        pozycja.idLokalizacji = lokalizacja.idLokalizacji??0;
                }

                button_import.Enabled = true;
            }
            catch (Exception e)
            {
                MessageBox.Show($@"Błąd czytania pliku: {e.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SprawdzStatusArtykulow()
        {
            foreach (PozycjaModel pozycja in pozycje)
            {
                decimal idArtykulu = 0;
                bool brak_kodu_EAN = true, brak_kodu_indywidualnego = true, brak_kodu_katalogowego = true;

                pozycja.brak_kodu_EAN = true;

                idArtykulu = ArtykulController.PobierzIdArtykuluJesliIstnieje(pozycja.kod_EAN??"",pozycja.kod_indywidualny??"",pozycja.kod_katalogowy??"",
                    pozycja.PART??"", ref brak_kodu_EAN, ref brak_kodu_indywidualnego, ref brak_kodu_katalogowego);

                if (idArtykulu != 0)
                {
                    pozycja.id_artykulu = idArtykulu;
                    pozycja.STATUS_POZYCJI = "Ok";
                }
                pozycja.brak_kodu_EAN = brak_kodu_EAN;
                pozycja.brak_kodu_indywidualnego = brak_kodu_indywidualnego;
                pozycja.brak_kodu_katalogowego = brak_kodu_katalogowego;
            }
        }

        private void button_listaKontrahentow_Click(object sender, EventArgs e)
        {
            Form_kontrahenci form_kontrahenci = new Form_kontrahenci();
            if (form_kontrahenci.ShowDialog() == DialogResult.Yes)
            {
                wybranyKontrahent = form_kontrahenci.wybranyKontrahent;
                textBox_kontrahent.Text = wybranyKontrahent.NAZWA;
            }
        }

        private void button_import_Click(object sender, EventArgs e)
        {
            try
            {
                PozycjaModel pozycja = (from p in pozycje
                                        where p.STATUS_POZYCJI != "Ok"
                                        select p).FirstOrDefault();

                if (pozycja != null) { MessageBox.Show("Istnieją pozycje nierozpoznane. Należy powiązać lub utworzyć artykuły.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                if (wybranyKontrahent == null) { MessageBox.Show("Wybierz kontrahenta.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                C_imag_dane_importu_zam_od_odbiorcyController.UsunStareDaneImportowe();
                //MessageBox.Show("twit check 1");
                C_imag_dane_importu_zam_od_odbiorcyController.WypelnijDaneImportowe(pozycje);
                //MessageBox.Show("twit check 2");
                if (MessageBox.Show("Czy aktualizować artykuły przed wystawieniem zamówienia danymi z pliku XLSX?", "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    int wynik = ArtykulController.ZaktualizujArtykuly();

                    if (wynik != -1)
                    {
                        string nrZamowienia = ZamowienieController.UtworzZamowienie(wybranyKontrahent.ID_KONTRAHENTA, dateTimePicker_dataDokumentu.Value);
                        MessageBox.Show($@"Zamówienie o numerze {nrZamowienia} zostało wystawione.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    string nrZamowienia = ZamowienieController.UtworzZamowienie(wybranyKontrahent.ID_KONTRAHENTA, dateTimePicker_dataDokumentu.Value);
                    MessageBox.Show($@"Zamówienie o numerze {nrZamowienia} zostało wystawione.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException!=null)
                    MessageBox.Show($@"Błąd wystawienia zamówienia: {ex.Message} {ex.InnerException.Message}.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show($@"Błąd wystawienia zamówienia: {ex.Message}.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dataGridView_pozycje_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "id_artykulu":
                    e.Column.Visible = false;
                    break;
                case "brak_kodu_EAN":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "Brak EAN w bazie";
                    break;
                case "brak_kodu_indywidualnego":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "Brak Kodu ind. w bazie";
                    break;
                case "brak_kodu_katalogowego":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "Brak Kodu kat. w bazie";
                    break;
                default:
                    e.Column.Visible = true;
                    break;
            }
        }

        private void dataGridView_pozycje_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            //dgv.AutoResizeColumns();
            //dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void button_powiaz_Click(object sender, EventArgs e)
        {
            if (dataGridView_pozycje.SelectedRows == null)
            {
                MessageBox.Show("Wskarz pozycję.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_pozycje.SelectedRows.Count != 0)
            {
                string kod_katalogowy = string.Empty;
                if (dataGridView_pozycje["kod_katalogowy", dataGridView_pozycje.SelectedRows[0].Index].Value != null) 
                    kod_katalogowy = dataGridView_pozycje["kod_katalogowy", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

                string kod_EAN = string.Empty;
                if (dataGridView_pozycje["kod_EAN", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                    kod_EAN = dataGridView_pozycje["kod_EAN", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

                string kod_indywidualny = string.Empty;
                if (dataGridView_pozycje["kod_indywidualny", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                    kod_indywidualny = dataGridView_pozycje["kod_indywidualny", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

                string PART = dataGridView_pozycje["PART", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();
                string STATUS_POZYCJI = dataGridView_pozycje["STATUS_POZYCJI", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

                if (STATUS_POZYCJI == "Ok") { MessageBox.Show("Pozycja jest powiązana.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                PozycjaModel pozycja = (from p in pozycje
                                        where (kod_EAN != string.Empty && p.kod_EAN == kod_EAN)
                                        || (kod_indywidualny != string.Empty && p.kod_indywidualny == kod_indywidualny)
                                        || (kod_katalogowy != string.Empty && p.kod_katalogowy == kod_katalogowy)
                                        select p).FirstOrDefault();

                Form_artykuly form_artykuly = new Form_artykuly();
                if (form_artykuly.ShowDialog() == DialogResult.Yes)
                {
                    decimal id_artykulu = form_artykuly.id_artykulu;

                    if ((kod_EAN ?? "") != string.Empty)
                    {
                        bool czyDomyslnyKodKreskowy = false;
                        if (MessageBox.Show("Czy ustawić kod kreskowy jako domyślny?", "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            czyDomyslnyKodKreskowy = true;

                        if (ArtykulController.DodajKodKresowy(id_artykulu, kod_EAN, czyDomyslnyKodKreskowy))
                        {
                            if (pozycje != null)
                            {
                                pozycja.STATUS_POZYCJI = "Ok";
                                pozycja.id_artykulu = id_artykulu;
                            }
                            dataGridView_pozycje.DataSource = null;
                            dataGridView_pozycje.DataSource = pozycje;
                        }
                    }
                    else
                    {
                        if (pozycja != null)
                        {
                            pozycja.STATUS_POZYCJI = "Ok";
                            pozycja.id_artykulu = id_artykulu;
                            //MessageBox.Show($@"{pozycja.kod_EAN},{pozycja.PART},{pozycja.STATUS_POZYCJI} - ustawiłem Ok");
                        }
                        dataGridView_pozycje.DataSource = null;
                        dataGridView_pozycje.DataSource = pozycje;
                    }
                }
            }
        }

        private void button_utworzArtykul_Click(object sender, EventArgs e)
        {
            if (dataGridView_pozycje.SelectedRows == null)
            {
                MessageBox.Show("Wskarz pozycję.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_pozycje.SelectedRows.Count != 0)
            {
                string kod_EAN = string.Empty;
                if (dataGridView_pozycje["kod_EAN", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                    kod_EAN = dataGridView_pozycje["kod_EAN", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

                string PART = string.Empty;
                try
                {
                    PART = dataGridView_pozycje["PART", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();
                }
                catch (Exception)
                {
                    MessageBox.Show("Wypełnij PART.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                string kod_indywidualny = string.Empty;
                string kod_katalogowy = string.Empty;

                if (dataGridView_pozycje["kod_indywidualny", dataGridView_pozycje.SelectedRows[0].Index].Value == null) kod_indywidualny = "";
                else
                    kod_indywidualny = dataGridView_pozycje["kod_indywidualny", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

                if (dataGridView_pozycje["kod_katalogowy", dataGridView_pozycje.SelectedRows[0].Index].Value == null) kod_katalogowy = "";
                else
                    kod_katalogowy = dataGridView_pozycje["kod_katalogowy", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

                string STATUS_POZYCJI = dataGridView_pozycje["STATUS_POZYCJI", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

                if (STATUS_POZYCJI == "Ok") { MessageBox.Show("Pozycja jest powiązana.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                PozycjaModel pozycja = (from p in pozycje
                                        where (p.kod_katalogowy??"") == kod_katalogowy && (p.kod_indywidualny??"") == kod_indywidualny && (p.kod_EAN??"") == kod_EAN && p.PART == PART
                                        select p).FirstOrDefault();

                if (pozycja != null)
                {
                    decimal id_artykulu = ArtykulController.UtworzArtykulIZwrocIdArtykulu(pozycja);
                    if (id_artykulu != 0)
                    {
                        pozycja.STATUS_POZYCJI = "Ok";
                        pozycja.id_artykulu = id_artykulu;

                        dataGridView_pozycje.DataSource = null;
                        dataGridView_pozycje.DataSource = pozycje;
                    }
                }
            }
        }

        private void button_zamknij_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView_pozycje_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            string STATUS_POZYCJI = dataGridView_pozycje["STATUS_POZYCJI", e.RowIndex].Value.ToString();

            bool brak_kodu_EAN = bool.Parse(dataGridView_pozycje["brak_kodu_EAN", e.RowIndex].Value.ToString());
            bool brak_kodu_indywidualnego = bool.Parse(dataGridView_pozycje["brak_kodu_indywidualnego", e.RowIndex].Value.ToString());
            bool brak_kodu_katalogowego = bool.Parse(dataGridView_pozycje["brak_kodu_katalogowego", e.RowIndex].Value.ToString());

            string kod_EAN = string.Empty;
            if (dataGridView_pozycje["kod_EAN", e.RowIndex].Value != null)
                kod_EAN = dataGridView_pozycje["kod_EAN", e.RowIndex].Value.ToString();

            string kod_indywidualny = string.Empty;
            if (dataGridView_pozycje["kod_indywidualny", e.RowIndex].Value != null)
                kod_indywidualny = dataGridView_pozycje["kod_indywidualny", e.RowIndex].Value.ToString();

            if (STATUS_POZYCJI == "Ok")
            {
                button_utworzArtykul.Enabled = button_powiaz.Enabled = false;
                if ((brak_kodu_EAN) && (kod_EAN != string.Empty)) button_dopiszKodEAN.Enabled = true; else button_dopiszKodEAN.Enabled = false;
                if ((brak_kodu_indywidualnego) && (kod_indywidualny != string.Empty)) button_dopiszKodIndywidualny.Enabled = true; else button_dopiszKodIndywidualny.Enabled = false;
            }
            else
            {
                button_utworzArtykul.Enabled = button_powiaz.Enabled = true;
                button_dopiszKodIndywidualny.Enabled = button_dopiszKodEAN.Enabled = false;
            }

        }

        private void button_dopiszKodEAN_Click(object sender, EventArgs e)
        {
            decimal id_artykulu = 0;
            if (dataGridView_pozycje["id_artykulu", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                id_artykulu = decimal.Parse(dataGridView_pozycje["id_artykulu", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString());

            string kod_EAN = string.Empty;
            if (dataGridView_pozycje["kod_EAN", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                kod_EAN = dataGridView_pozycje["kod_EAN", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

            List<PozycjaModel> pozycjeModyfikowane = (from p in pozycje
                                    where p.id_artykulu == id_artykulu && p.kod_EAN == kod_EAN
                                    select p).ToList();

            bool czyDomyslnyKodKreskowy = false;
            if (MessageBox.Show("Czy ustawić kod kreskowy jako domyślny?", "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                czyDomyslnyKodKreskowy = true;

            if (ArtykulController.DodajKodKresowy(id_artykulu, kod_EAN, czyDomyslnyKodKreskowy))
            {
                foreach(var p in pozycjeModyfikowane)
                {
                    p.brak_kodu_EAN = false;
                }
                dataGridView_pozycje.DataSource = null;
                dataGridView_pozycje.DataSource = pozycje;
            }
        }

        private void button_dopiszKodIndywidualny_Click(object sender, EventArgs e)
        {
            decimal id_artykulu = 0;
            if (dataGridView_pozycje["id_artykulu", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                id_artykulu = decimal.Parse(dataGridView_pozycje["id_artykulu", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString());

            string part = string.Empty;
            if (dataGridView_pozycje["PART", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                part = dataGridView_pozycje["PART", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

            string nazwa = string.Empty;
            if (dataGridView_pozycje["nazwa", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                nazwa = dataGridView_pozycje["nazwa", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

            PozycjaModel pozycja = (from p in pozycje
                                    where p.id_artykulu == id_artykulu && p.PART == part
                                    select p).FirstOrDefault();


            if (pozycja != null)
            {
                CennikIndywidualnyController.DopisLubAktualizujIndekIndywidualny(pozycja);

                pozycja.brak_kodu_indywidualnego = false;
                dataGridView_pozycje.DataSource = null;
                dataGridView_pozycje.DataSource = pozycje;
            }
        }

        private void button_dodajKodKontrahenta_Click(object sender, EventArgs e)
        {
            decimal id_artykulu = 0;
            if (dataGridView_pozycje["id_artykulu", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                id_artykulu = decimal.Parse(dataGridView_pozycje["id_artykulu", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString());

            string part = string.Empty;
            if (dataGridView_pozycje["PART", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                part = dataGridView_pozycje["PART", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

            string nazwa = string.Empty;
            if (dataGridView_pozycje["nazwa", dataGridView_pozycje.SelectedRows[0].Index].Value != null)
                nazwa = dataGridView_pozycje["nazwa", dataGridView_pozycje.SelectedRows[0].Index].Value.ToString();

            PozycjaModel pozycja = (from p in pozycje
                                    where p.id_artykulu == id_artykulu && p.PART == part
                                    select p).FirstOrDefault();


            if (pozycja != null)
            {
                try
                {
                    CennikKontrahentaController.DopisLubAktualizujIndekKontrahenta(pozycja);
                    MessageBox.Show("Indeks kontrahenta został zapisany.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                        MessageBox.Show($@"Błąd zapisu indeksu kontrahenta: {ex.Message} {ex.InnerException.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show($@"Błąd zapisu indeksu kontrahenta: {ex.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
