﻿namespace importPZ
{
    partial class Form_kontrahenci
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_wszystko = new System.Windows.Forms.Button();
            this.button_szukaj = new System.Windows.Forms.Button();
            this.textBox_ciagNazwy = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_wybierz = new System.Windows.Forms.Button();
            this.button_anuluj = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView_kontrahenci = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_kontrahenci)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_wszystko);
            this.panel1.Controls.Add(this.button_szukaj);
            this.panel1.Controls.Add(this.textBox_ciagNazwy);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(802, 57);
            this.panel1.TabIndex = 0;
            // 
            // button_wszystko
            // 
            this.button_wszystko.Location = new System.Drawing.Point(419, 16);
            this.button_wszystko.Name = "button_wszystko";
            this.button_wszystko.Size = new System.Drawing.Size(75, 23);
            this.button_wszystko.TabIndex = 3;
            this.button_wszystko.Text = "Wszystko";
            this.button_wszystko.UseVisualStyleBackColor = true;
            this.button_wszystko.Click += new System.EventHandler(this.button_wszystko_Click);
            // 
            // button_szukaj
            // 
            this.button_szukaj.Location = new System.Drawing.Point(338, 17);
            this.button_szukaj.Name = "button_szukaj";
            this.button_szukaj.Size = new System.Drawing.Size(75, 23);
            this.button_szukaj.TabIndex = 2;
            this.button_szukaj.Text = "Szukaj";
            this.button_szukaj.UseVisualStyleBackColor = true;
            this.button_szukaj.Click += new System.EventHandler(this.button_szukaj_Click);
            // 
            // textBox_ciagNazwy
            // 
            this.textBox_ciagNazwy.Location = new System.Drawing.Point(123, 19);
            this.textBox_ciagNazwy.Name = "textBox_ciagNazwy";
            this.textBox_ciagNazwy.Size = new System.Drawing.Size(209, 20);
            this.textBox_ciagNazwy.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Szukaj po nazwie:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_wybierz);
            this.panel2.Controls.Add(this.button_anuluj);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 396);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(802, 75);
            this.panel2.TabIndex = 1;
            // 
            // button_wybierz
            // 
            this.button_wybierz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_wybierz.Location = new System.Drawing.Point(618, 24);
            this.button_wybierz.Name = "button_wybierz";
            this.button_wybierz.Size = new System.Drawing.Size(75, 23);
            this.button_wybierz.TabIndex = 1;
            this.button_wybierz.Text = "Wybierz";
            this.button_wybierz.UseVisualStyleBackColor = true;
            this.button_wybierz.Click += new System.EventHandler(this.button_wybierz_Click);
            // 
            // button_anuluj
            // 
            this.button_anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_anuluj.Location = new System.Drawing.Point(699, 24);
            this.button_anuluj.Name = "button_anuluj";
            this.button_anuluj.Size = new System.Drawing.Size(75, 23);
            this.button_anuluj.TabIndex = 0;
            this.button_anuluj.Text = "Anuluj";
            this.button_anuluj.UseVisualStyleBackColor = true;
            this.button_anuluj.Click += new System.EventHandler(this.button_anuluj_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView_kontrahenci);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 57);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(802, 339);
            this.panel3.TabIndex = 2;
            // 
            // dataGridView_kontrahenci
            // 
            this.dataGridView_kontrahenci.AllowUserToAddRows = false;
            this.dataGridView_kontrahenci.AllowUserToDeleteRows = false;
            this.dataGridView_kontrahenci.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_kontrahenci.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_kontrahenci.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_kontrahenci.Name = "dataGridView_kontrahenci";
            this.dataGridView_kontrahenci.ReadOnly = true;
            this.dataGridView_kontrahenci.Size = new System.Drawing.Size(802, 339);
            this.dataGridView_kontrahenci.TabIndex = 0;
            this.dataGridView_kontrahenci.DataSourceChanged += new System.EventHandler(this.dataGridView_kontrahenci_DataSourceChanged);
            this.dataGridView_kontrahenci.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_kontrahenci_ColumnAdded);
            // 
            // Form_kontrahenci
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 471);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form_kontrahenci";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Kontrahenci";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_kontrahenci)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView_kontrahenci;
        private System.Windows.Forms.Button button_anuluj;
        private System.Windows.Forms.Button button_wybierz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_szukaj;
        private System.Windows.Forms.TextBox textBox_ciagNazwy;
        private System.Windows.Forms.Button button_wszystko;
    }
}