﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using importPZ.Controllers;
using importPZ.Models;

namespace importPZ
{
    public partial class Form_artykuly : Form
    {
        private List<ArtykulModel> artykuly;

        public decimal id_artykulu = 0;

        public Form_artykuly()
        {
            InitializeComponent();
            ZaladujArtykulyDoSiatki();
            UstawPoleDoWyszukiwania();
        }

        private void ZaladujArtykulyDoSiatki()
        {
            artykuly = ArtykulController.PobierzArtykuly();
            dataGridView_artykuly.DataSource = artykuly;
        }

        private void UstawPoleDoWyszukiwania()
        {
            comboBox_pole.Text = "INDEKS_KATALOGOWY";
        }

        private void dataGridView_artykuly_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "NAZWA":
                    e.Column.Visible = true;
                    break;
                case "INDEKS_KATALOGOWY":
                    e.Column.Visible = true;
                    break;
                case "INDEKS_HANDLOWY":
                    e.Column.Visible = true;
                    break;
                case "KOD_KRESKOWY":
                    e.Column.Visible = true;
                    break;
                case "KOD_KRESKOWY_DOMYSLNY":
                    e.Column.Visible = true;
                    break;
                default:
                    e.Column.Visible = false;
                    break;
            }
        }

        private void dataGridView_artykuly_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void button_wszystko_Click(object sender, EventArgs e)
        {
            dataGridView_artykuly.DataSource = artykuly;
        }

        private void button_szukaj_Click(object sender, EventArgs e)
        {
            List<ArtykulModel> artykulyFiltrowane = null;

            if (comboBox_pole.Text == "INDEKS_HANDLOWY")
            {
                artykulyFiltrowane = artykuly.Where(x => x.INDEKS_HANDLOWY.ToUpper().Contains(textBox_szukany.Text.ToUpper())).ToList();
            }
            else if (comboBox_pole.Text == "INDEKS_KATALOGOWY")
            {
                artykulyFiltrowane = artykuly.Where(x => x.INDEKS_KATALOGOWY.ToUpper().Contains(textBox_szukany.Text.ToUpper())).ToList();
            }
            else if (comboBox_pole.Text == "KOD_KRESKOWY")
            {
                artykulyFiltrowane = artykuly.Where(x => x.KOD_KRESKOWY.ToUpper().Contains(textBox_szukany.Text.ToUpper()) || x.KOD_KRESKOWY_DOMYSLNY.ToUpper().Contains(textBox_szukany.Text.ToUpper())).ToList();
            }
            else if (comboBox_pole.Text == "NAZWA")
            {
                artykulyFiltrowane = artykuly.Where(x => x.NAZWA.ToUpper().Contains(textBox_szukany.Text.ToUpper())).ToList();
            }

            if (artykulyFiltrowane != null)
                dataGridView_artykuly.DataSource = artykulyFiltrowane;
        }

        private void button_anuluj_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_przypisz_Click(object sender, EventArgs e)
        {
            if (dataGridView_artykuly.SelectedRows == null)
            {
                MessageBox.Show("Wskarz artykuł.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_artykuly.SelectedRows.Count != 0)
            {
                id_artykulu = decimal.Parse(dataGridView_artykuly["ID_ARTYKULU", dataGridView_artykuly.SelectedRows[0].Index].Value.ToString());
                if (id_artykulu != 0)
                    this.DialogResult = DialogResult.Yes;
                Close();
            }
        }
    }
}
