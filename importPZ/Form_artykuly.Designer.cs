﻿namespace importPZ
{
    partial class Form_artykuly
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_wszystko = new System.Windows.Forms.Button();
            this.button_szukaj = new System.Windows.Forms.Button();
            this.textBox_szukany = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_pole = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView_artykuly = new System.Windows.Forms.DataGridView();
            this.button_anuluj = new System.Windows.Forms.Button();
            this.button_przypisz = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_artykuly)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_wszystko);
            this.panel1.Controls.Add(this.button_szukaj);
            this.panel1.Controls.Add(this.textBox_szukany);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBox_pole);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(836, 84);
            this.panel1.TabIndex = 0;
            // 
            // button_wszystko
            // 
            this.button_wszystko.Location = new System.Drawing.Point(521, 32);
            this.button_wszystko.Name = "button_wszystko";
            this.button_wszystko.Size = new System.Drawing.Size(75, 23);
            this.button_wszystko.TabIndex = 4;
            this.button_wszystko.Text = "Wszystko";
            this.button_wszystko.UseVisualStyleBackColor = true;
            this.button_wszystko.Click += new System.EventHandler(this.button_wszystko_Click);
            // 
            // button_szukaj
            // 
            this.button_szukaj.Location = new System.Drawing.Point(440, 32);
            this.button_szukaj.Name = "button_szukaj";
            this.button_szukaj.Size = new System.Drawing.Size(75, 23);
            this.button_szukaj.TabIndex = 3;
            this.button_szukaj.Text = "Szukaj";
            this.button_szukaj.UseVisualStyleBackColor = true;
            this.button_szukaj.Click += new System.EventHandler(this.button_szukaj_Click);
            // 
            // textBox_szukany
            // 
            this.textBox_szukany.Location = new System.Drawing.Point(231, 35);
            this.textBox_szukany.Name = "textBox_szukany";
            this.textBox_szukany.Size = new System.Drawing.Size(203, 20);
            this.textBox_szukany.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Wyszukiwanie:";
            // 
            // comboBox_pole
            // 
            this.comboBox_pole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_pole.FormattingEnabled = true;
            this.comboBox_pole.Items.AddRange(new object[] {
            "INDEKS_HANDLOWY",
            "INDEKS_KATALOGOWY",
            "KOD_KRESKOWY",
            "NAZWA"});
            this.comboBox_pole.Location = new System.Drawing.Point(26, 34);
            this.comboBox_pole.Name = "comboBox_pole";
            this.comboBox_pole.Size = new System.Drawing.Size(199, 21);
            this.comboBox_pole.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_przypisz);
            this.panel2.Controls.Add(this.button_anuluj);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 420);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(836, 76);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView_artykuly);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 84);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(836, 336);
            this.panel3.TabIndex = 2;
            // 
            // dataGridView_artykuly
            // 
            this.dataGridView_artykuly.AllowUserToAddRows = false;
            this.dataGridView_artykuly.AllowUserToDeleteRows = false;
            this.dataGridView_artykuly.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_artykuly.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_artykuly.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_artykuly.Name = "dataGridView_artykuly";
            this.dataGridView_artykuly.ReadOnly = true;
            this.dataGridView_artykuly.Size = new System.Drawing.Size(836, 336);
            this.dataGridView_artykuly.TabIndex = 0;
            this.dataGridView_artykuly.DataSourceChanged += new System.EventHandler(this.dataGridView_artykuly_DataSourceChanged);
            this.dataGridView_artykuly.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_artykuly_ColumnAdded);
            // 
            // button_anuluj
            // 
            this.button_anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_anuluj.Location = new System.Drawing.Point(722, 24);
            this.button_anuluj.Name = "button_anuluj";
            this.button_anuluj.Size = new System.Drawing.Size(75, 23);
            this.button_anuluj.TabIndex = 0;
            this.button_anuluj.Text = "Anuluj";
            this.button_anuluj.UseVisualStyleBackColor = true;
            this.button_anuluj.Click += new System.EventHandler(this.button_anuluj_Click);
            // 
            // button_przypisz
            // 
            this.button_przypisz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_przypisz.Location = new System.Drawing.Point(641, 24);
            this.button_przypisz.Name = "button_przypisz";
            this.button_przypisz.Size = new System.Drawing.Size(75, 23);
            this.button_przypisz.TabIndex = 1;
            this.button_przypisz.Text = "Przypisz";
            this.button_przypisz.UseVisualStyleBackColor = true;
            this.button_przypisz.Click += new System.EventHandler(this.button_przypisz_Click);
            // 
            // Form_artykuly
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(836, 496);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form_artykuly";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Artykuły";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_artykuly)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView_artykuly;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_pole;
        private System.Windows.Forms.Button button_szukaj;
        private System.Windows.Forms.TextBox textBox_szukany;
        private System.Windows.Forms.Button button_wszystko;
        private System.Windows.Forms.Button button_anuluj;
        private System.Windows.Forms.Button button_przypisz;
    }
}